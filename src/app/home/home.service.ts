import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ApiDispatcherService } from '../core/api-dispatcher.service';

@Injectable()
export class HomeService {
    constructor(private http: HttpClient,
        private _apiDispatcher: ApiDispatcherService
    ) {}
}

