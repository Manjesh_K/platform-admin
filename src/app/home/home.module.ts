import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home.component';
import { ObjNgFor } from '../core/objNgFor.pipe';
import { ApplicationCardComponent } from '../components/application-card/application-card.component';
import { UserTypeCardComponent } from '../components/user-type-card/user-type-card.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
    declarations: [
        HomeComponent,
        ApplicationCardComponent,
        UserTypeCardComponent,
        ObjNgFor
    ],
    imports: [
        CommonModule,
        TranslateModule
    ]
})
export class HomeModule {

}
