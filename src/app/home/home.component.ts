import {
    Component
} from '@angular/core';
import { Router } from '@angular/router';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { UsersService } from '../users/users.service';
import { CommonUtilService } from '../core/common-util.service';
import { AppService } from '../core/app.service';
@Component({
    selector: 'pz-home',
    templateUrl: './home.component.html',
    styleUrls: [
        './home.style.scss'
    ]
})
export class HomeComponent implements OnInit {
    users = [];
    applications = [];
    constructor(
        private router: Router,
        private _appSvc: AppService,
        private usersService: UsersService,
        private utilService: CommonUtilService
    ) { }
    onNavigateToUsers() {
        this.router.navigate(['users']);
    }
    ngOnInit() {
        const applications = this._appSvc.getApplicationsList();
        const users = this.usersService.getUsersCount();
        forkJoin([applications, users]).subscribe(results => {
            this.applications = this.utilService.groupBy(results[0], (c) => c.type);
            this.users = results[1];
        });
    }
}
