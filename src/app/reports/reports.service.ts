import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ReportService {

    constructor(private http: HttpClient) { }

    getReportData = () => {
        const path = '../../assets/mock/reports.json';
        return this.http.get(path);
    }

}
