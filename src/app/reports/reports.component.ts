import { Component, OnInit } from '@angular/core';
import { ReportService } from './reports.service';

@Component({
    selector: 'pz-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
    reports: any = [];

    constructor(private reportSvc: ReportService) { }

    ngOnInit() {
        this.reportSvc.getReportData().subscribe(data => {
            this.reports = data;
        });
    }

    pageChanged(event: any): void {}

}
