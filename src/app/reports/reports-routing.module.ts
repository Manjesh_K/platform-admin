import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportsComponent } from './reports.component';
import { AuthGuard } from '../auth.guard';

const reportsRoutes: Routes = [
    {
        path: 'reports',
        canActivate: [AuthGuard],
        component: ReportsComponent,
        data: {
            permission: {
                only: ['UserReport_UserReport'],
                redirectTo: 'home'
            }
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(reportsRoutes)
    ],
    exports: [RouterModule]
})
export class ReportsRoutingModule {

}
