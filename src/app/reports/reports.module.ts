// import core modules
import {
    NgModule
} from '@angular/core';
import {
    CommonModule
} from '@angular/common';
import {
    PaginationModule
} from 'ngx-bootstrap';

// import custom module
import {
    ReportsComponent
} from './reports.component';
import {
    ReportsRoutingModule
} from './reports-routing.module';
import {
    TranslateModule
} from '@ngx-translate/core';

import { ReportService } from './reports.service';


@NgModule({
    declarations: [
        ReportsComponent
    ],
    imports: [
        CommonModule,
        PaginationModule.forRoot(),
        ReportsRoutingModule,
        TranslateModule
    ],
    providers: [
        ReportService
    ]
})
export class ReportsModule {

}
