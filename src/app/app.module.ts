import { BrowserModule } from '@angular/platform-browser';
import {
    BrowserAnimationsModule
} from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import {
    HttpClientModule,
    HttpClient,
    HTTP_INTERCEPTORS
} from '@angular/common/http';
import {
    HttpModule
} from '@angular/http';
import {
    TranslateModule,
    TranslateLoader
} from '@ngx-translate/core';
import {
    TranslateHttpLoader
} from '@ngx-translate/http-loader';

import {
    CachingInterceptor
} from './caching.interceptor';
import {
    RequestCache,
    RequestCacheWithMap
} from './request-cache.service';
import {
    MessageService
} from './message.service';

import { AppComponent } from './app.component';
import { AppConfig } from './config/app.config';
import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from './auth/auth.module';
import { HomeModule } from './home/home.module';
import { ReportsModule } from './reports/reports.module';
import { CoreModule } from './core/core.module';
import { SelectBoxComponent } from './components/select-box/select-box.component';
import { GenericComponent } from './generic/generic.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from './components/components.module';
const APP_PROVIDERS: any[] = [
    AppConfig,
    {
        provide: APP_INITIALIZER,
        useFactory: (appConfig: AppConfig) => () => appConfig.loadAppConfig(),
        deps: [AppConfig],
        multi: true
    },
    {
        provide: HTTP_INTERCEPTORS,
        useClass: CachingInterceptor,
        multi: true
    },
    {
        provide: RequestCache,
        useClass: RequestCacheWithMap
    }
];

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        GenericComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        ComponentsModule,
        HttpModule,
        HttpClientModule,
        AppRoutingModule,
        AuthModule,
        HomeModule,
        ReportsModule,
        CoreModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        })
    ],
    providers: [
        AppConfig,
        MessageService,
        APP_PROVIDERS
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
