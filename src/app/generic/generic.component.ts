import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
    selector: 'pz-generic',
    templateUrl: './generic.component.html',
    styleUrls: ['./generic.component.scss']
})
export class GenericComponent implements OnInit {
    public fGroup:FormGroup;
    public formControl: FormControl = new FormControl([], Validators.required);
    public roles: any[] = [
        {
            name: 'SALES_OPS',
            code: 'SALES_OPS'
        },
        {
            name: 'SALES',
            code: 'SALES'
        },
        {
            name: 'CSR',
            code: 'CSR'
        },
        {
            name: 'FINANCE',
            code: 'FINANCE'
        },
        {
            name: 'APPROVER',
            code: 'APPROVER'
        }
    ];
    constructor() { }

    ngOnInit() {
        this.fGroup = new FormGroup({
            formControl: this.formControl
        })
    }

}
