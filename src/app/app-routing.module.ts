import {
    NgModule
} from '@angular/core';

import {
    RouterModule,
    Routes,
    PreloadAllModules
} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { UserDetailComponent } from './users/user-detail/user-detail.component';
import { PageNotFoundComponent } from './core/page-not-found/page-not-found.component';
import { AuthGuard } from './auth.guard';
import { GenericComponent } from './generic/generic.component';
const ROUTES: any[] = [
    {
        path: '',
        redirectTo: '/home', pathMatch: 'full'
    },
    {
        path: 'generic',
        component: GenericComponent
    },
    {
        path: 'home',
        canActivate: [AuthGuard],
        component: HomeComponent,
        pathMatch: 'full'
    },
    {
        path: 'users',
        canActivate: [AuthGuard],
        loadChildren: './users/users.module#UsersModule',
        data: {
            permission: {
                only: ['ViewUser'],
                redirectTo: 'home'
            }
        }
    }
    // },
    // {
    //     path: 'not-found',
    //     component: PageNotFoundComponent
    // },
    // {
    //     path: '**',
    //     redirectTo: '/not-found'
    // }
];
@NgModule(
    {
        imports: [
            RouterModule.forRoot(
                ROUTES, {
                    useHash: false,
                    preloadingStrategy: PreloadAllModules
                }
            )
        ],
        exports: [
            RouterModule
        ]
    }
)
export class AppRoutingModule {
}
