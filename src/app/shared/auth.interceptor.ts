import {
    Injectable
} from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { EventEmitterService } from '../core/event-emitter.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    private _pendingRequestCount: number = 0;
    constructor(
        private _eventEmitter: EventEmitterService
    ) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (0 >= this._pendingRequestCount) {
            const showLoader = true;
            this._eventEmitter.onLoaderToggle.emit(showLoader);
        }
        ++this._pendingRequestCount;
        const hostName:string = window.location.hostname;
        const httpHeaders: any = {
            'domain': 'localhost' === hostName ? '52.53.121.120' : window.location.hostname
        };
        const bearerToken = localStorage.getItem('bearerToken');
        if (bearerToken) {
            httpHeaders['Authorization'] = bearerToken;
        }
        const copiedReq = req.clone({
            setHeaders: httpHeaders
        });
        return next.handle(copiedReq)
            .catch(error => {
                if (401 === error.status) {
                    this._eventEmitter.onSessionExpired.emit(error);
                    return Observable.empty();
                }
                return Observable.of(error);
            })
            .finally(() => {
                --this._pendingRequestCount;
                if (0 >= this._pendingRequestCount) {
                    const showLoader = false;
                    this._pendingRequestCount = 0;
                    this._eventEmitter.onLoaderToggle.emit(showLoader);
                }
            });
    }
}
