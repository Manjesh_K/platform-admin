import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ChangeDetectorRef,
    OnChanges,
    HostListener,
    ElementRef,
    ViewChild
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { CommonUtilService } from '../../core/common-util.service';
import { BsDropdownDirective } from 'ngx-bootstrap';
import { debounceTime } from 'rxjs/operators/debounceTime';
import { distinctUntilChanged } from 'rxjs/operators/distinctUntilChanged';

@Component({
    selector: 'pz-select-box',
    templateUrl: './select-box.component.html',
    styleUrls: ['./select-box.component.scss']
})
export class SelectBoxComponent implements OnInit, OnChanges {
    @Input()
    public contaimerClass: string;
    @Input()
    public pzFrm: FormGroup;
    @Input()
    public pzFrmControl: FormControl;
    @Input()
    public pzControlLabel: string;
    @Input()
    public items: any[];
    @Input()
    public isDisabled: boolean;
    @Input()
    public isDropup: boolean;
    @Input()
    public isMultiSelect: boolean = false;
    @Input()
    public selectCode: string = 'code';
    @Input()
    public disableOnSelect: boolean = false;
    @Input()
    public displayKey: string = 'name';
    @Input()
    public displayBadges: boolean = false;
    @Output()
    public onItemSelect: EventEmitter<any> = new EventEmitter();
    @Output()
    public loadData: EventEmitter<string> = new EventEmitter();
    @Input()
    public formSubmitted: boolean = false;
    @Input()
    public searchLabel: string;
    @Input()
    public serverSideSearch: boolean = false;
    @Input()
    public editable: boolean = true;
    @Input()
    public isRequired: boolean = false;
    @ViewChild('dropdown')
    public dropdown: BsDropdownDirective;
    public filter: FormControl = new FormControl('');
    public searchControl: FormControl = new FormControl('');
    public selectBoxUid: string = 'PZ' + new Date().toISOString();
    public selectedItems: any[];
    public selectedValue: string;
    private _selectedValue: any;
    constructor(
        private _changeRef: ChangeDetectorRef,
        private _commonUtilSvc: CommonUtilService,
        private _eleRef: ElementRef
    ) {
    }

    ngOnChanges() {
        if (
            this._commonUtilSvc.isValidArray(this.items) &&
            !this._commonUtilSvc.isValidString(this.selectedValue)
        ) {
            if (!this._commonUtilSvc.isValidString(this._selectedValue)) {
                this._selectedValue = this.pzFrmControl.value;
            }
            this._displayControlValue(this._selectedValue);
            this._setSelectedItems();
        }
    }

    ngOnInit() {
        if (this._commonUtilSvc.isValidString(
            this.pzFrmControl.value
        )
        ) {
            this._setValue(this.pzFrmControl.value);
        }
        const errors: any = this.pzFrmControl.errors || {};
        this.isRequired = errors.required;
        this.pzFrmControl.valueChanges.subscribe((value) => {
            if (this._commonUtilSvc.isValidString(value) || this._commonUtilSvc.isValidArray(value)) {
                this._setValue(value);
            } else {
                this.selectedValue = '';
            }
        });
        if (this.serverSideSearch) {
            this.searchControl.valueChanges
                .debounceTime(500)
                .distinctUntilChanged()
                .subscribe((value: string) => {
                    if (this._commonUtilSvc.isValidString(value)) {
                        this.loadData.emit(value);
                    } else {
                        this.items = [];
                    }
                });
        }
    }

    @HostListener('document:click', ['$event'])
    public onClick($event) {
        if (this._eleRef.nativeElement.contains($event.target)) {
            return;
        }
        this.dropdown.hide();
    }
    public openDropdown(): void {
        this.dropdown.show();
    }
    public deSelectItem(item: any) {
        if (this.isMultiSelect) {
            this.selectItem(item);
        } else {
            this.selectedItems = [];
            item.selected = false;
            delete item.disabled;
            this.pzFrmControl.setValue('');
            this.onItemSelect.emit({
                selectedItem: item,
                controlValue: item[this.selectCode]
            });
        }
    }
    public selectItem(item: any) {
        if (item.disabled || item.hideElement) {
            return;
        }
        let selectedItems: any;
        if (this.isMultiSelect) {
            item.selected = !item.selected;
            selectedItems = this._getSelectedItems();
        } else {
            this.selectedItems = [
                item
            ];
            selectedItems = item[this.selectCode];
            this.selectedValue = item[this.displayKey];
            this.dropdown.hide();
        }
        if (this.serverSideSearch) {
            this.searchControl.setValue('');
        }
        this.pzFrmControl.setValue(selectedItems);
        /* this.pzFrmControl.updateValueAndValidity(); */
        this.onItemSelect.emit({
            selectedItem: item,
            controlValue: selectedItems
        });
    }
    private _displayControlValue(value: any): any {
        let values: string[];
        let items: any[] = this.items;
        if (
            this._commonUtilSvc.isUnDefined(value) ||
            !this._commonUtilSvc.isValidArray(items)
        ) {
            return;
        }
        values = this._commonUtilSvc.isValidArray(value) ?
            value :
            [value];
        let selectedValue: string;
        items.forEach((item: any) => {
            let key: string;
            let value: string;
            if (this._commonUtilSvc.isObject(item)) {
                key = item[this.selectCode];
                value = item[this.displayKey];
            } else {
                key = value = item;
            }
            if (-1 === values.indexOf(key)) {
                item.selected = false;
                if (!item.hideElement) {
                    delete item.disabled;
                }
                return;
            }
            if (this._commonUtilSvc.isObject(item)) {
                item.selected = true;
                if (this.disableOnSelect) {
                    item.disabled = true;
                }
            }
            selectedValue = this._commonUtilSvc.isValidString(selectedValue) ?
                selectedValue + ', ' + value :
                value;
        });
        this.selectedValue = selectedValue;
    }
    private _getSelectedItems(): string[] {
        this._setSelectedItems();
        const selectedItems: string[] = [];
        this.selectedItems.forEach((item: any) => {
            selectedItems.push(item[this.selectCode]);
        });
        return selectedItems;
    }
    private _setSelectedItems(): void {
        const items: any[] = this.items.filter((item: any) => {
            return true === item.selected;
        });
        this.selectedItems = items;
    }
    private _setValue(value: any) {
        if (this._commonUtilSvc.isValidArray(this.items)) {
            this._displayControlValue(value);
        } else {
            this._selectedValue = value;
        }
    }
}
