import { Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  FormGroup,
  FormControl,
  FormArray
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonUtilService } from '../../core/common-util.service';
import { UsersService } from '../../users/users.service';

@Component({
  selector: 'pz-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit, OnDestroy {
  public filterForm: FormGroup;
  private filtersSubscription: Subscription;
  public groupedFilters: any;
  @Output() closeMenu: EventEmitter<string> = new EventEmitter();
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private commonUtil: CommonUtilService,
    private usersService: UsersService
  ) { }

  ngOnInit() {
    this.groupedFilters = [
      {
        'field': 'userType',
        'fieldLabel': 'USER_TYPE',
        'options': [
          {
            'label': 'TENANT',
            'value': 'TENANT'
          },
          {
            'label': 'SHOPUSER',
            'value': 'SHOPUSER'
          },
          {
            'label': 'CUSTOMER',
            'value': 'CUSTOMER'
          }
        ]
      },
      {
        'field': 'userStatus',
        'fieldLabel': 'USER_STATUS',
        'options': [
          {
            'label': 'ACTIVE',
            'value': 'ACTIVE'
          },
          {
            'label': 'INACTIVE',
            'value': 'INACTIVE'
          },
          {
            'label': 'APPROVAL_PENDING',
            'value': 'APPROVAL_PENDING'
          },
          {
            'label': 'REJECT',
            'value': 'REJECT'
          }
        ]
      },
      {
        'field': 'userAssociationStatus',
        'fieldLabel': 'USER_ASSOCIATION_STATUS',
        'options': [
          {
            'label': 'APPROVAL_PENDING',
            'value': 'APPROVAL_PENDING'
          },
          {
            'label': 'REJECT',
            'value': 'REJECT'
          }
        ]
      },
      {
        'field': 'accountLocked',
        'fieldLabel': 'ACCOUNT_LOCKED',
        'options': [
          {
            'label': 'LOCKED',
            'value': 'true'
          },
          {
            'label': 'UNLOCKED',
            'value': 'false'
          }
        ]
      }
    ];
    const config = {};
    for (const group of this.groupedFilters) {
      const formArray = new FormArray([]);
      for (const option of group.options) {
        const defaultValue = this.getDefaultFormControlValue(group.field, option.value);
        const formControl = new FormControl(defaultValue, []);
        formArray.push(formControl);
      }
      config[group.field] = formArray;
    }
    this.filterForm = new FormGroup(config);
    // watch form data
    this.filterForm.valueChanges.subscribe(
      (formValues) => {
        const qParams: {} = {};
        for (const key of Object.keys(formValues)) {
          const options = this.commonUtil.getFilteredItem(this.groupedFilters, 'field', key).options;
          const selectedOptions = [];
          for (let i = 0; i < formValues[key].length; i++) {
            if (formValues[key][i] === true) {
              selectedOptions.push(options[i].value);
            }
          }
          qParams[key] = selectedOptions.slice();
        }
        for (const key of Object.keys(qParams)) {
          const strSepByPipe = qParams[key].join('|');
          qParams[key] = strSepByPipe ? strSepByPipe : null;
        }
        this.router.navigate([], {queryParams: qParams, queryParamsHandling: 'merge'});
      }
    );

    this.filtersSubscription = this.usersService.filtesReset.subscribe(
      () => {
        this.resetForm();
      }
    );
  }

  private getDefaultFormControlValue(field, optionValue): boolean {
    const fieldParams = this.route.snapshot.queryParams[field];
    if (fieldParams) {
      const fieldParamsArr = fieldParams.split('|');
      return fieldParamsArr.indexOf(optionValue) !== -1 ;
    } else {
      return false;
    }
  }

  resetForm() {
    this.filterForm.reset();
  }

  onSubmit() {}

  close() {
    this.closeMenu.emit('in');
  }

  ngOnDestroy() {
    this.closeMenu.unsubscribe();
    this.filtersSubscription.unsubscribe();
  }
}
