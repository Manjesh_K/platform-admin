import { Pipe, PipeTransform } from '@angular/core';
import { FormControl } from '@angular/forms';

@Pipe({
    name: 'search'
})
export class SearchPipe implements PipeTransform {

    transform(items: any[], field?:string, searchTerm?: string): any[] {
        if (!items || '' === searchTerm || !searchTerm) {
            return items;
        }
        if (!items || !searchTerm || '' === searchTerm) {
            return items;
        }
        return items.filter((item: any) => {
            const itemVal = item[field].toLowerCase();
            return -1 !== itemVal.indexOf(searchTerm.toLowerCase());
        });
    }

}
