import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'pz-navbar-fixed-bottom',
  templateUrl: './navbar-fixed-bottom.component.html',
  styleUrls: ['./navbar-fixed-bottom.component.scss']
})
export class NavbarFixedBottomComponent implements OnInit {
  @Input() public buttons: any[];
  public leftButtons: any[];
  public rightButtons: any[];
  @Output()
  public buttonClicked: EventEmitter<any> = new EventEmitter();
  constructor() {
   }

  ngOnInit() {}

  btnClicked(button) {
    if (button.action === 'updateUser' || button.action === 'createUser' || button.action === 'approveUser') {
      return;
    } else if (button.action === 'rejectUser' || button.action === 'deactivateUser') {
      this.buttonClicked.emit({'action': button.action});
    } else {
      this.buttonClicked.emit({'action': button.action});
    }
  }

}
