import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectBoxComponent } from './select-box/select-box.component';
import { TranslateModule } from '@ngx-translate/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchPipe } from './search.pipe';
import { FeatureToggleDirective } from './feature-toggle.directive';

@NgModule({
    imports: [
        CommonModule,
        BsDropdownModule.forRoot(),
        ReactiveFormsModule,
        TranslateModule
    ],
    declarations: [
        SelectBoxComponent,
        SearchPipe,
        FeatureToggleDirective
    ],
    exports: [
        SelectBoxComponent,
        FeatureToggleDirective
    ]
})
export class ComponentsModule { }
