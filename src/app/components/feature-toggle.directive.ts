/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Directive,
    ElementRef,
    Renderer2,
    Input,
    OnInit
} from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Directive({
    selector: '[pzFeatureToggle]'
})
export class FeatureToggleDirective implements OnInit {
    @Input()
    public pzFeatureToggle: boolean;
    @Input()
    public permission: string;
    constructor(
        private _authSvc: AuthService,
        private _el: ElementRef,
        private _renderer: Renderer2
    ) { }

    public ngOnInit() {
        if (this._authSvc.userHasPermissions(this.permission)) {
            return;
        }
        if (this.pzFeatureToggle) {
            this._renderer.removeChild(this._el.nativeElement.parentNode, this._el.nativeElement);
            // this._el.nativeElement.remove(); // there is no sopport for remove method on IE 11
        } else {
            this._renderer.addClass(this._el.nativeElement, 'disabled');
        }
    }
}
