import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'pz-user-type-card',
  templateUrl: './user-type-card.component.html',
  styleUrls: ['./user-type-card.component.scss']
})
export class UserTypeCardComponent implements OnInit {
  @Input() type;
  @Input() count;
  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
  }

  onSelect() {
    const qParams = {
      'userStatus': this.type === 'TOTAL' || this.type === 'DISABLED' || this.type === 'ENABLED' ? null : this.type,
      'accountLocked': this.type === 'DISABLED' || this.type === 'ENABLED' ? (this.type === 'DISABLED' ? 'true' : 'false') : null,
      'page': 1,
      'size': 9
    };
    this.router.navigate(['users'], {queryParams: qParams});
  }

}
