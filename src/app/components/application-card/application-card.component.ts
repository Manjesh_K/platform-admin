import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'pz-application-card',
  templateUrl: './application-card.component.html',
  styleUrls: ['./application-card.component.scss']
})
export class ApplicationCardComponent implements OnInit {
  @Input() label;
  @Input() name;
  constructor() { }

  ngOnInit() {
  }

  onSelect() {
    //this.router.navigate([this.user.customerNumber], { relativeTo: this.route});
  }

}
