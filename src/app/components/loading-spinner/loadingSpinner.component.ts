/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    Input
} from '@angular/core';

import { EventEmitterService } from '../../core/event-emitter.service';

@Component({
    selector: 'pz-loading-spinner',
    template: `
        <div *ngIf="showLoader" class="loading-spinner-dim-overlay"></div>
        <div *ngIf="showLoader" class="loading-spinner text-pzprimary">
            <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
            <span class="sr-only">Loading...</span>
        </div>
    `,
    styles: [
        `.loading-spinner-dim-overlay {
            position: fixed;
            background: #333;
            opacity: 0.3;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 1051;
        }`,
        `.loading-spinner {
            position: fixed;
            opacity: 1;
            top: 50%;
            left: 50%;
            width: 93px;
            height: 72px;
            z-index: 1052;
            font-size: 2em;
            margin-left: -46.5px;
            margin-top: -36px;
        }`
    ]
})
export class LoadingSpinnerComponent {
    public showLoader: boolean = false;
    public disableLoader: boolean;
    constructor(eventEmitter: EventEmitterService) {
        eventEmitter.onLoaderToggle
            .subscribe((showLoader) => {
                if (this.disableLoader) {
                    this.showLoader = false;
                    this.disableLoader = false;
                } else {
                    this.showLoader = showLoader;
                }
            });
        eventEmitter.onLoaderDisabled.subscribe(
            (disableLoader: boolean) => this.disableLoader = disableLoader
        );
    }
}
