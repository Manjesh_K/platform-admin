/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import * as apiEndPoints from './envSettings.json';

import {
    TranslateService
} from '@ngx-translate/core';

@Injectable()
export class AppConfig {
    private appConfig: any = null;
    private _apiEndPoints: any = null;
    private envSettings: any = null;
    private _appFeatures: any[];
    private _appModules: any[];
    constructor(private _http: HttpClient, private _translateSvc: TranslateService) {
        this._setLanguageSettings();
    }
    /**
     * @returns The selected market
     */
    get _selectedMarket(): any {
        const selectedMarket: any = this._getSelectedMarket();
        return null !== selectedMarket && 'object' === typeof selectedMarket && selectedMarket.marketId ?
            selectedMarket : {};
    }
    /**
     * @returns The selected market id
     */
    get _marketId(): string {
        const selectedMarket: any = this._selectedMarket;
        const marketId: string = selectedMarket.marketId;
        return ('string' === typeof marketId && marketId.length) ? marketId : 'dana-NA';
    }
    /**
     * @returns The selected locale id
     */
    get _localeId(): string {
        const selectedMarket: any = this._selectedMarket;
        const localeId: string = selectedMarket.localeId;
        return ('string' === typeof localeId && localeId.length) ? localeId : 'en_US';
    }

    set _selectedMarket(selectedMarket: any) {
        if (!selectedMarket || 'object' !== typeof selectedMarket) {
            selectedMarket = {};
        }
        sessionStorage.setItem('selectedMarket', JSON.stringify(selectedMarket));
    }

    /**
     * Get api end Point for a given micro service
     */

    public getApiEndPoint(microServiceName: string): string {
        try {
            return this._apiEndPoints[microServiceName];
        } catch (e) {
            return null;
        }
    }

    /**
     * Get api end Points
     */

    public getApiEndPoints(): any {
        return this.envSettings.apiEndPoints;
    }
    /**
     * @getter: To get the list of countries
     * @returns {T|*}
     * @private
     */
    get _countryList(): any[] {
        try {
            return this.getAppFeatureSettings('countryList').list;
        } catch (e) {
            return [];
        }
    }

    public getAppFeatureSettings(featureName: string, moduleFeatures?: any[]): any {
        const featureSettings = this.getAppFeature(featureName, moduleFeatures)['settings'];
        return featureSettings || {};
    }

    /**
     * @desc: To get application settings of a given specific module
     * @param {*} moduleName 
     * @param {*} key  
     */
    public getModuleSettings(moduleName: string, key: string) {
        if (!moduleName || !moduleName.length) {
            throw 'Please provide a valid name';
        }
        let moduleSettings: any = this.getAppModule(moduleName);
        return key && key.length ? moduleSettings[key] : moduleSettings;
    }
    /**
         * @desc: To get application settings of a given specific module
         * @param {*} moduleName 
         * @param {*} key 
         */
    public getAppModuleFeatureSettings(moduleName: string, featureName: string): any {
        const module: any = this.getAppModule(moduleName);
        const feature: any = this.getAppFeature(featureName, module.features) || {};
        return feature['settings'];
    }
    public getAppModule(moduleName: string): any {
        const appModules = this._appModules;
        if (!appModules || !appModules.length) {
            return { enable: true };
        }
        for (const module of appModules) {
            if (!module || moduleName !== module.name) {
                continue;
            }
            return module;
        }
        return { enable: true };
    }

    public getAppFeature(featureName: string, moduleFeatures: any[]): any {
        const features: any[] = moduleFeatures && moduleFeatures.length ? moduleFeatures : this._appFeatures;
        if (!features) {
            return {};
        }
        for (const feature of features) {
            if (!feature || featureName !== feature.name) {
                continue;
            }
            return feature;
        }
        return {};
    }
    /**
     * Use to get the data found in the second file (config file)
     */
    public getConfig(key: any) {
        return this.appConfig[key];
    }

    /**
     * Use to get the data found in the first file (env file)
     */
    public getEnvSettings(key: any) {
        return this.envSettings[key];
    }

    /**
     * This method:
     * a) Loads "env.json" to get the current working environment
     * (e.g.: 'production', 'development')
     * b) Loads "config.[env].json" to get all env's variables
     * (e.g.: 'config.development.json')
     */
    public loadAppConfig() {
        return new Promise((resolve, reject) => {
            this._http.get('/env/env.json?v=' + new Date().getTime())
                .subscribe(
                    data => {
                        this._setEnvSettings(data);
                        const apiPath = this.getEnvSettings('protocol') + '//'
                            + this.getApiEndPoint('platform')
                            + '/platform-service/api/v1/application/appconfig/domain';
                        this._http.get(apiPath)
                            .subscribe((appConfig: any) => {
                                this.appConfig = appConfig;
                                try {
                                    this._appFeatures = appConfig.appInfo.applicationConfig.appFeatures;
                                    this._appModules = appConfig.appInfo.applicationConfig.modules;
                                } catch (e) {
                                    console.log(e);
                                }
                                resolve(true);
                            });
                    },
                    error => {
                        resolve(false);
                        return Observable.throw(error.json().error || 'Server error');
                    }
                );
        });
    }
    private _getSelectedMarket(): any {
        try {
            return JSON.parse(sessionStorage.getItem('selectedMarket'));
        } catch (e) {
            return undefined;
        }
    }
    private _setEnvSettings(envSettings: any): void {
        const envName: string = envSettings.envName;
        const apiEndPointsObj = apiEndPoints['apiEndPoints'];
        const winLocation = window.location;
        this._apiEndPoints = Object.assign(
            {},
            apiEndPointsObj[envName],
            envSettings.apiEndPoints
        );
        this.envSettings = {
            apiKey: envSettings.apiKey,
            protocol: winLocation.protocol,
            envName: envSettings.envName,
            hostName: winLocation.hostname
        };
    }

    private _setLanguageSettings() {
        this._translateSvc.addLangs(['en', 'fr']);
        this._translateSvc.setDefaultLang('en');
        const browserLang = this._translateSvc.getBrowserLang();
        this._translateSvc.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    }
}
