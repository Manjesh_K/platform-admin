import { Component, ViewEncapsulation, OnInit} from '@angular/core';
import { AuthService } from './auth/auth.service';
import { EventEmitterService } from './core/event-emitter.service';

@Component({
  selector: 'pz-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  public collapse: boolean;
  public isAuthenticated: boolean;
  constructor(
    private authService: AuthService,
    private eventEmitter: EventEmitterService
  ) {}
  ngOnInit() {
    this.collapse = window.innerWidth > 767 ? false : true;
    this.isAuthenticated = this.authService.isAuthenticated;
    this.eventEmitter.onUserLoginStateChange
      .subscribe(
      (response) => {
        if (response) {
          this.isAuthenticated = true;
        } else {
          this.isAuthenticated = false;
        }
      }
    );
  }
}
