import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import { AuthComponent } from './auth.component';
import { AuthGuard } from '../auth.guard';

const authRoutes: Routes = [
    { path: 'signin', component: SigninComponent},
    { path: 'auth', component: AuthComponent}
];

@NgModule({
    imports: [
        RouterModule.forChild(authRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AuthRoutingModule {}
