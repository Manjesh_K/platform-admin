import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { UsersService } from '../../users/users.service';
import { AuthService } from '../auth.service';
import { CommonUtilService } from '../../core/common-util.service';
import { NavigationService } from '../../core/navigation.service';

@Component({
    selector: 'pz-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
    public doRemember: boolean = true;
    public signin: any = { username: null, password: null };
    @ViewChild('signinForm')
    private _signinForm: NgForm;
    constructor(
        private _router: Router,
        private _usersService: UsersService,
        private _authService: AuthService,
        private _commonUtil: CommonUtilService,
        private _navigation: NavigationService,
        private _activatedRoute: ActivatedRoute
    ) {
        const loginInfo = _commonUtil.getCokie('rememberMe');
        if (loginInfo && _commonUtil.isObject(loginInfo)) {
            Object.assign(this.signin, loginInfo);
        }
    }

    ngOnInit() {
        this._activatedRoute.params.subscribe(
            (params: Params) => {
                if (this._authService.isAuthenticated) {
                    this._navigation.navigate('/home');
                }
            }
        );
    }

    onSignin(form: NgForm) {
        if (this._signinForm.invalid) {
            this._commonUtil.setFormDirty(this._signinForm.controls);
            return;
        }
        if (this.doRemember) {
            this._commonUtil.setCookie('rememberMe', JSON.stringify(this.signin),
                365, undefined, undefined, true);
        } else {
            this._commonUtil.deleteCokie('rememberMe');
        }
        const reqPayload = {
            user: this.signin.username.trim(),
            password: this.signin.password
        };
        this._authService.login(reqPayload).subscribe((response: any) => {
            if (response['status'] === 'SUCCESS') {
                let redirectionUrl: string = this._authService.redirectionUrl;
                if (!this._commonUtil.isValidString(redirectionUrl) || '/signin' === redirectionUrl) {
                    redirectionUrl = '/';
                }
                this._navigation.navigate(redirectionUrl);
            } else {
                console.log('error while signing in!!!');
            }
        }, (error: any) => {
            // this._navigation.navigate('/home');
        });
    }
}
