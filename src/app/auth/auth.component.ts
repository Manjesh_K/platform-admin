import { Component } from '@angular/core';
import { AuthService } from './auth.service';
import { NavigationService } from '../core/navigation.service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { ActivatedRoute, Params } from '@angular/router';
import { CommonUtilService } from '../core/common-util.service';
import { AppConfig } from '../config/app.config';

@Component({
    selector: 'pz-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
    private _jwtToken: string;
    constructor(
        private _appConfig: AppConfig,
        private _authService: AuthService,
        private _navigation: NavigationService,
        private _activatedRoute: ActivatedRoute,
        private _commonUtil: CommonUtilService
    ) {
        this._activatedRoute.queryParams.subscribe((params: Params) => {
            const token: string = params['token'];
            this._setSelectedMarket(params['appName'], params['locale']);
            if (_commonUtil.isValidString(token)) {
                this._jwtToken = token;
                const routeTo = params['refUrl'];
                this._jwtLogin(routeTo);
            } else {
                this._authService
                    .invalidateSession()
                    .then(() => {
                        _navigation.navigate('./signin');
                    });
            }
        });
    }
    public ngOnInit() {
        console.log('AuthComponent loaded!');
    }
    private _doJwtLogin(routeTo) {
        this._authService
            .jwtLogin(this._jwtToken)
            .subscribe(
                () => {
                    if (this._commonUtil.isValidString(routeTo)) {
                        this._navigation.navigate(routeTo);
                    } else {
                        this._navigation.navigate('./');
                    }
                },
                () => {
                    console.log('Display login failed message.');
                    this._authService
                        .invalidateSession()
                        .then(() => {
                            this._navigation.navigate('./signin');
                        });
                }
            );
    }
    private _jwtLogin(routeTo): void {
        if (this._authService.isAuthenticated) {
            this._authService
                .invalidateSession()
                .then(() => {
                    this._doJwtLogin(routeTo);
                });
        } else {
            this._doJwtLogin(routeTo);
        }
    }
    private _setSelectedMarket(appName: string, locale: string): void {
        if (
            !this._commonUtil.isValidString(appName)
        ) {
            appName = this._appConfig._marketId;
        }
        if (!this._commonUtil.isValidString(locale)) {
            locale = this._appConfig._localeId;
        }
        this._appConfig._selectedMarket = {
            marketId: appName,
            localeId: locale,
            switchLocale: false,
            isPreferred: true
        };
    }
}
