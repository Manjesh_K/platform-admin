import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from '../core/local-storage.service';
import { CommonUtilService } from '../core/common-util.service';
import { Observable } from 'rxjs/Observable';
import { CacheFactoryService } from '../core/cache-factory.service';
import { EventEmitterService } from '../core/event-emitter.service';
import { ApiDispatcherService } from '../core/api-dispatcher.service';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';


@Injectable()
export class AuthService extends LocalStorageService {
    private _bearerToken: string = null;
    private _redirectionUrl: string;
    private _userPermissions: string[] = [];
    private _userRoles: string[] = [];
    private _userInfo: any = {};
    constructor(
        public commonUtils: CommonUtilService,
        private _router: Router,
        private _cacheFactory: CacheFactoryService,
        private _eventEmitter: EventEmitterService,
        private _apiDispatcher: ApiDispatcherService,
        private _http: HttpClient
    ) {
        super(commonUtils);
    }
    get jwt(): boolean {
        return this.getItem('jwt');
    }
    /**
     * Returns bearer token
     */
    get bearerToken(): string {
        if (!this._bearerToken) {
            this._bearerToken = localStorage.getItem('bearerToken');
        }
        return this._bearerToken;
    }
    /**
     * Check if user is authenticated
     */
    get isAuthenticated() {
        return this.bearerToken ? true : false;
    }
    /**
     * Invalidate user session
     */
    public invalidateSession(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this._cacheFactory.deleteAllItems();
            this.deleteAllItems();
            this._bearerToken = null;
            this._eventEmitter
                .onUserLoginStateChange
                .emit(
                false
                );
            resolve(true);
        });
    }
    /**
     * Login from jwt token
     * @param token
     */
    public jwtLogin(token: any): Observable<any> {
        this._setBearerToken(token);
        return this._setUserContext(true);
    }
    private _setUserContext(jwt: boolean = false): Observable<any> {
        return this._apiDispatcher.doPostApiCall(`jwtLogin`, undefined)
        .map(
            (response: any) => {
                if (response['status'] === 'SUCCESS') {
                    this._setUserPermissions(response.permissions);
                    this._setUserRoles(response.roles);
                    this._setUserInfo(response);
                    this.setItem('jwt', jwt);
                    this._eventEmitter.onUserLoginStateChange.emit(true);
                }
                return response;
            }
        );
    }
    /**
     * Login the given user
     * @param reqPayload
     */
    public login(reqPayload: any): Observable<any> {
        return this._apiDispatcher.doPostApiCall(`login`, reqPayload)
        .map(
            (response: any) => {
                if (response['status'] === 'SUCCESS') {
                this._setBearerToken(response.message);
                this._setUserPermissions(response.permissions);
                this._setUserRoles(response.roles);
                this._setUserInfo(response);
                this.setItem('jwt', false);
                this._eventEmitter.onUserLoginStateChange.emit(true);
                }
                return response;
            }
        );
    }

    get redirectionUrl(): string {
        return this._redirectionUrl;
    }

    set redirectionUrl(url: string) {
        this._redirectionUrl = url;
    }

    /**
     * Login from the given auth token
     * @param token
     */
    public loginFromAuthToken(token: string) {
        // Will login from auth token here
    }
    /**
     * Check if user has a given permission
     * @param permission
     */
    public userHasPermissions(permission): boolean {
        const userPermissions = this._getUserPermissions();
        return -1 !== userPermissions.indexOf(permission);
    }
    /**
     * Check if user has a given role
     * @param role 
     */
    public userHasRole(role:string): boolean {
        const roles = this._getUserRoles();
        return -1 !== roles.indexOf(role);
    }

    private _getUserPermissions(): string[] {
        if (this.commonUtil.isValidArray(this._userPermissions)) {
            return this._userPermissions;
        }
        const userPermissions: string[] =
            this.getItem('upKey', true);
        this._userPermissions =
            this.commonUtil.isValidArray(userPermissions)
                ? userPermissions
                : [];
        return this._userPermissions;
    }

    private _setBearerToken(token: string) {
        if (!token) {
            return;
        }
        this._bearerToken = 'Bearer ' + token;
        this.setItem('bearerToken', this._bearerToken);
    }

    private _setUserPermissions(userPermissions: string[]): void {
        if (!this.commonUtil.isValidArray(userPermissions)) {
            userPermissions = [];
        }
        this._userPermissions = userPermissions;
        this.setItem('upKey', userPermissions, true);
    }

    private _setUserRoles(userRoles: string[]): void {
        this._userRoles = userRoles;
        this.setItem('urKey', userRoles, true);
    }

    private _getUserRoles(): string[] {
        if (this.commonUtil.isValidArray(this._userRoles)) {
            return this._userRoles;
        }
        const userRoles: string[] =
            this.getItem('urKey', true);
        this._userRoles =
            this.commonUtil.isValidArray(userRoles)
                ? userRoles
                : [];

        return this._userRoles;
    }

    private _setUserInfo(response: any): void {
        this._userInfo = {
            firstName: response.firstName,
            lastName: response.lastName,
            email: response.email,
            userId: response.userId
        };
        this.setItem('uiKey', this._userInfo, true);
    }

    public getUserInfo(): any {
        if (this.commonUtils.isValidObject(this._userInfo)) {
            return this._userInfo;
        }
        const userInfo: any =
            this.getItem('uiKey', true);
        this._userInfo = this.commonUtils.isValidObject(userInfo) ? userInfo : {};
        return this._userInfo;
    }
}
