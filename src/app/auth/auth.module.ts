import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { SigninComponent } from './signin/signin.component';
import { AuthComponent } from './auth.component';
import { AuthRoutingModule } from './auth-routing.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        SigninComponent,
        AuthComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        AuthRoutingModule,
        TranslateModule
    ],
    providers: []
})
export class AuthModule {

}
