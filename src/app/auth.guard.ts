import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild, NavigationExtras, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { CommonUtilService } from './core/common-util.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(
        private _authService: AuthService,
        private router: Router,
        private _authSvc: AuthService,
        private _commonUtil: CommonUtilService) {
        }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        const qParams: Params = route.queryParams;
        const navigationExtras: NavigationExtras = {};
        if (Object.keys(qParams).length) {
            navigationExtras.queryParams = qParams;
        }
        const stateUrl: string = state.url;
        if (this._authService.isAuthenticated) {
            const permission = route.data['permission'];
            const params = route.params;
            let canActivate: boolean = true;
            if (this._commonUtil.isValidObject(permission) && this._commonUtil.isValidArray(permission.only)) {
                let cta:string;
                if (stateUrl.includes('save') && params.userId === 'new') {
                    cta = permission.only[1];
                } else {
                    cta = permission.only[0];
                }
                canActivate = this._authService.userHasPermissions(cta);
            } else {
                canActivate = true;
            }
            if (!canActivate) {
                this.router.navigate(['/home']);
            }
            return canActivate;
        } else {
            if (stateUrl.includes('signin')) {
                return true;
            } else {
                this._authService.redirectionUrl = stateUrl;
                this.router.navigate(['/signin', navigationExtras]);
                return false;
            }
        }
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.canActivate(route, state);
    }
}
