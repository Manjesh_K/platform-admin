import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, AbstractControl } from '@angular/forms';
import { AppService } from '../../core/app.service';
import { CommonUtilService } from '../../core/common-util.service';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AppConfig } from '../../config/app.config';
import { UsersService } from '../users.service';
import { User } from '../user.model';
import { AuthService } from '../../auth/auth.service';
import { EventEmitterService } from '../../core/event-emitter.service';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { ActionModalComponent } from '../action-modal/action-modal.component';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    selector: 'pz-save-user',
    templateUrl: './save-user.component.html',
    styleUrls: ['./save-user.component.scss']
})
export class SaveUserComponent extends User implements OnInit, OnDestroy {
    public actionButtons: any[];
    public actionModal: BsModalRef;
    public askBeforeRedirect: boolean = true;
    public isAccountLocked: boolean;
    public includeUsername: boolean;
    public showActionButtons: boolean = false;
    public statusLogs: any[] = [];
    // List of countires
    public countries: any[] = [

    ];
    public collapseCardData: any = {
        card1: {
            isCollapsed: false,
        },
        card2: {
            isCollapsed: true,
        },
        card3: {
            isCollapsed: true,
        }
    };
    // List of states
    public states: any[] = [

    ];
    // List of cities
    public cities: any[] = [

    ];
    public countryCodes: any[];
    public apps: any[] = [];
    public custAccounts: any[] = [];
    public formSubmitted: boolean = false;
    public isRequired: boolean = false;
    public modes: any[] = [
        {
            name: 'Mobile',
            code: 'Mobile Phone'
        },
        {
            name: 'Email',
            code: 'Email'
        },
        {
            name: 'Phone',
            code: 'Work Phone'
        }
    ];
    public renderForm: boolean = false;
    public roles: any = {};
    public rolesGroup: any = {
        appRoles: [

        ],
        customerAccRoles: [

        ]
    };
    public usrFrm: FormGroup;
    public usrFrmData: any = {
    };
    public selectedValue: string;
    public dpOpen: boolean = false;
    public userId: string;
    public userDetails: any;
    private _defaultUserType: string = 'Internal';
    private _modalConfig = {
        animated: true,
        class: 'pz-modal-popup action-modal',
        keyboard: false,
        backdrop: true,
        ignoreBackdropClick: true
    };
    private _actionModalSubscription: Subscription;
    constructor(
        private _appConfig: AppConfig,
        private _appSvc: AppService,
        private _authSvc: AuthService,
        private _modalService: BsModalService,
        private _commonUtilSvc: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _router: ActivatedRoute,
        private _userSvc: UsersService,
        private _routerSvc: Router,
        private _changeDetectRef: ChangeDetectorRef
    ) {
        super();
        // Check if userId field is required
        const userIdConfig = _appConfig.getAppFeatureSettings(
            'userIdConfiguration'
        ) || {};
        this.includeUsername = userIdConfig.isCustomized;

        _router.params.subscribe((params: Params) => {
            this.userId = params.userId;
            this._getUserDetails();
        });
        // this._setActionButtons();
        this._setApps();
        this._setCountryCodes();
        this._setCountryList();
    }

    ngOnInit() {
        this._actionModalSubscription = this._userSvc.actionModalActionComplete.subscribe(
            (actionData: any) => {
                const action = actionData.action;
                if (
                    action === 'removeCustomer'
                ) {
                    actionData.status = null;
                    this._updateCustomerAccRoles(actionData);
                } else if (
                    action === 'approveCustomer' || 
                    action === 'rejectCustomer'
                ) {
                    this.updateCustomerStatus(actionData);
                } else if (
                    action === 'deactivateUser' ||
                    action === 'approveUser' ||
                    action === 'rejectUser'
                ) {
                    this.askBeforeRedirect = false;
                    this._routerSvc.navigate(['/users', this.userId]);
                } else if ('REMOVE_APP_ROLES' === action) {
                    actionData.status = null;
                    this._updateAppRoles(actionData);
                }
            }
        );
    }
    public addNewAppRole(group: any, editable: boolean = true): void {
        const roleGroup: any = {
            roles: group.data === 'promotion' ?
                this._commonUtilSvc.getClonedArray(this.roles.appRoles['PILM']) :
                this._commonUtilSvc.getClonedArray(this.roles.appRoles[group.data]),
            group: {
                label: 'USER_FORM.FIELDS.APP_NAME.LABEL',
                selectCode: 'name',
                displayKey: 'displayName',
                isDropup: true,
                editable: editable,
                control: new FormControl(
                    group.data,
                    Validators.required
                )
            },
            role: {
                label: 'USER_FORM.FIELDS.ROLES.LABEL',
                SEARCH_LABEL: 'USER_FORM.FIELDS.ROLES.SEARCH_ROLE',
                isDropup: true,
                editable: this._commonUtilSvc.isValidString(group.data),
                control: new FormControl(group.roles, Validators.required),
                isMultiSelect: true
            },
            actions: {
                canBeRemoved: 'new' !== this.userId && 'APPROVAL_PENDING' !== group.status && 'APPROVAL_PENDING' !== this.userDetails.status,
                status: group.status
            }
        }; 
        
        let roleGroups: FormArray = this.appRoles;
        if (this._commonUtilSvc.isObject(
            roleGroups
        )
        ) {
            roleGroup.pos = this.rolesGroup.appRoles.length;
            roleGroups.push(
                new FormGroup({
                    group: roleGroup.group.control,
                    roles: roleGroup.role.control
                })
            );
        } else if (this.usrFrm && editable) {
            this.appRoles = new FormArray([
                roleGroup.group.control,
                roleGroup.role.control
            ]);
            this.usrFrm.controls['appRoles'] = this.appRoles;
            this.appRoles.updateValueAndValidity();
        }
        const item: any = this._commonUtilSvc.getFilteredItem(
            this.apps,
            'selected',
            true
        );
        if (this._commonUtilSvc.isObject(item)) {
            item.hideElement = true;
        }
        this.rolesGroup.appRoles.unshift(roleGroup);
    }
    public removeAppRole(event: any):void {
        if (!this._commonUtilSvc.isObject(event)) {
            return;
        }
        if (this._commonUtilSvc.isValidString(event.status)) {
            this._showModal(event);
        } else {
            this._updateAppRoles(event);
        }
    }
    public addNewCustomerRole(group: any, editable: boolean = true): void {
        if ('INACTIVE' === group.status) {
            return;
        }
        const value: string = this._commonUtilSvc.isValidString(group.displayName) ?
            group.data + ': ' + group.displayName + ': ' + group.source :
            '';
        const roleGroup: any = {
            roles: this._commonUtilSvc.getClonedArray(this.roles.custAccountRoles),
            group: {
                label: 'USER_FORM.FIELDS.CUST_ACC.LABEL',
                SEARCH_LABEL: 'USER_FORM.FIELDS.CUST_ACC.SEARCH_CUST',
                control: new FormControl(value, Validators.required),
                selectCode: 'code',
                displayKey: 'name',
                isDropup: true,
                editable: editable,
                serverSideSearch: true
            },
            role: {
                label: 'USER_FORM.FIELDS.ROLES.LABEL',
                SEARCH_LABEL: 'USER_FORM.FIELDS.ROLES.SEARCH_ROLE',
                control: new FormControl(group.roles, []),
                isMultiSelect: true,
                selectCode: 'code',
                displayKey: 'name',
                isDropup: true,
                editable: true
            },
            actions: {
                status: this._commonUtilSvc.isValidString(value) ? group.status : null,
                isAuthorizedUser: group.isAuthorizedUser,
                canBeRemoved: 'new' === this.userId ? false : true,
                userStatus: this.userDetails ? this.userDetails.status : null
            }
        };
        if (group.status === 'REJECT') {
            roleGroup.role.control.clearValidators();
        } else {
            roleGroup.role.control.setValidators([Validators.required]);
        }
        roleGroup.role.control.updateValueAndValidity();
        let roleGroups: FormArray = this.customerAccRoles;
        if (this._commonUtilSvc.isObject(
            roleGroups
        )
        ) {
            roleGroup.pos = this.rolesGroup.customerAccRoles.length;
            roleGroups.push(
                new FormGroup({
                    group: roleGroup.group.control,
                    roles: roleGroup.role.control
                })
            );
        } else if (this.usrFrm && editable && !this.customerAccRoles) {
            this.customerAccRoles = new FormArray([
                new FormGroup({
                    group: roleGroup.group.control,
                    roles: roleGroup.role.control
                })
            ]
            );
            this.usrFrm.controls['customerAccRoles'] = this.customerAccRoles;
            this.customerAccRoles.updateValueAndValidity();
        }
        this.rolesGroup.customerAccRoles.unshift(roleGroup);
    }
    updateCustomerStatus(event) {
        const status: string = event.action === 'approveCustomer' ? 'ACTIVE' : 'REJECT';
        const roleGroup: any = this.rolesGroup.customerAccRoles[event.index];

        if (!this._commonUtilSvc.isObject(roleGroup)) {
            return;
        }
        // Update role group status
        roleGroup.actions.status = status;

        if (status === 'REJECT') {
            roleGroup.role.control.clearValidators();
            roleGroup.role.control.updateValueAndValidity();
        }
    }
    public removeCustomerRole(event: any) {
        if (!event.status && event.action === 'removeCustomer') {
            this._updateCustomerAccRoles(event);
        } else {
            this.buttonClickedHandler(event);
        }
    }
    public createFormControls() {

        if (this.includeUsername) {
            // Since userId is used for an alternate purpose, will be using 'username' as the form field // to create userId form field
            this.username = new FormControl(
                this.userDetails.userId,
                Validators.required
            );
        }

        this.emailId = new FormControl(
            this.userDetails.emailId,
            [Validators.required,
            Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)],
            this.validateEmailNotTaken.bind(this));
        // this.emailId = new FormControl(
        //     this.userDetails.emailId,
        //     [Validators.required,
        //     Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]);
        this.firstName = new FormControl(this.userDetails.firstName, Validators.required);
        this.lastName = new FormControl(this.userDetails.lastName, Validators.required);
        this.userType = new FormControl(this.userDetails.userType, Validators.required);
        this.address1 = new FormControl(this.userDetails.address1);
        this.address2 = new FormControl(this.userDetails.address2);
        this.companyName = new FormControl(this.userDetails.companyName);
        this.zip = new FormControl(this.userDetails.zip);
        this.country = new FormControl(this.userDetails.country, Validators.required);
        this.city = new FormControl(this.userDetails.city);
        if (!this._commonUtilSvc.isValidString(this.userDetails.city)) {
            this.city.disable();
        }
        this.state = new FormControl(this.userDetails.state);
        this.mobileCd = new FormControl('+1');
        this.phonenoCd = new FormControl('+1');
        this.faxCd = new FormControl('+1');
        this.mobile = new FormControl(this.userDetails.mobile);
        this.phoneno = new FormControl(this.userDetails.phoneno);
        this.fax = new FormControl(this.userDetails.fax);
        this.preferredContactType = new FormControl(this.userDetails.preferredContactType);
        const roleCards: any = {
            appRoles: [],
            customerAccRoles: []
        };
        for (let key in this.rolesGroup) {
            if (!this.rolesGroup.hasOwnProperty(key)) {
                continue;
            }
            const roleGroups: any[] = this.rolesGroup[key];
            if (!this._commonUtilSvc.isValidArray(roleGroups)) {
                continue;
            }
            roleGroups.forEach((roleGroup: any, index: number) => {
                /* const ctrlId:number = roleCards[key].length;
                roleGroup.group.uuid = ctrlId;
                roleGroup.role.uuid = ctrlId + 1; */
                roleGroup.pos = index;
                roleCards[key].push(
                    new FormGroup({
                        group: roleGroup.group.control,
                        roles: roleGroup.role.control
                    })
                );
            });
            this[key] = new FormArray(roleCards[key]);
        }
        if ('CUSTOMER' === this.userDetails.userType) {
            this.isRequired = true;
            this._setValidators();
        } else {
            this.isRequired = false;
        }
    }
    /* {
        name: 'SALES_OPS'
    },
    {
        name: 'SALES'
    },
    {
        name: 'CSR'
    } */
    public createFormGroup() {
        const fbGroup: any = {
            /* userType: this.userType,
            emailId: this.emailId, */
            firstName: this.firstName,
            lastName: this.lastName,
            address1: this.address1,
            address2: this.address2,
            companyName: this.companyName,
            zip: this.zip,
            country: this.country,
            state: this.state,
            city: this.city,
            mobileCd: this.mobileCd,
            phonenoCd: this.phonenoCd,
            faxCd: this.faxCd,
            fax: this.fax,
            preferredContactType: this.preferredContactType,
            mobile: this.mobile,
            phoneno: this.phoneno/* ,
            appRoles: this.appRoles,
            customerAccRoles: this.customerAccRoles */
        };
        if ('new' === this.userId) {
            fbGroup['userType'] = this.userType;
        }
        if ('new' === this.userId || this.includeUsername) {
            fbGroup['emailId'] = this.emailId;
        }
        if (this.includeUsername) {
            fbGroup['userId'] = this.username;
        }
        if (this._commonUtilSvc.isValidArray(this.rolesGroup.appRoles)) {
            fbGroup['appRoles'] = this.appRoles;
        }
        if (this._commonUtilSvc.isValidArray(this.rolesGroup.customerAccRoles)) {
            fbGroup['customerAccRoles'] = this.customerAccRoles;
        }
        this.usrFrm = new FormGroup(fbGroup);
    }
    public showCountryList() {
        this.dpOpen = true;
    }
    public checkValue() {
    }
    public onCountrySelect($event) {
        this._setSelectedCountry($event.controlValue);
        this.state.setValue('');
        this.city.setValue('');
        this.city.disable();
    }
    public onStateSelect($event) {
        // this._setSelectedState($event.controlValue);
        this.city.setValue('');
        this.city.enable();
    }

    public saveUser($event) {
        this.usrFrm.updateValueAndValidity();
        this._commonUtilSvc.delayCallback('saveUser', () => {
            this._doSaveUser();
        }, 100);
    }

    private _doSaveUser(): void {
        this.formSubmitted = true;
        if (this.usrFrm.invalid) {
            return;
        }
        const userType: string = this.userDetails.userType;
        const requestPayload: any = this._userSvc.getRequestPayLoad(this.usrFrm.value, userType);
        let emailId: string;

        if (!this.includeUsername) {
            requestPayload['userId'] = requestPayload['emailId'];
        }

        if ('new' === this.userId) {
            emailId = requestPayload['emailId'];
        } else {
            if (this.userDetails.status === 'APPROVAL_PENDING') {
                if (!this._commonUtilSvc.isValidArray(requestPayload.authorizations)) {
                    alert('Please add Customer Account(s) & Roles.');
                    return;
                }
                const event: any = {
                    'action': 'approveUser',
                    'text': 'APPROVE',
                    'roles': requestPayload.authorizations[0].roles,
                    'status': this.userDetails.status,
                    'authorizations': requestPayload.authorizations[0]
                };
                this.buttonClickedHandler(event);
                return;
            }
            requestPayload['userType'] = userType;
            requestPayload['emailId'] = this.userDetails.emailId;
            emailId = this.userDetails.emailId;
        }
        this._userSvc.saveUser(requestPayload, this.userId)
            .subscribe((response: any) => {
                if (!response.status || 0 === response.type) {
                    return;
                }
                if (400 === response.status || 500 === response.status) {
                    this._eventEmitter
                        .onAlert
                        .emit({
                            type: 'alert-error',
                            msgKey: 'ALERT.SERVER_ERR'
                        });
                    return;
                }
                const alertMsg = 'new' === this.userId ? 'ALERT.USER_CREATED' : 'ALERT.USER_UPDATED';
                this._eventEmitter
                    .onAlert
                    .emit({
                        type: 'alert-success',
                        msgKey: alertMsg
                    });
                const userId = 'new' === this.userId ? requestPayload['userId'] : this.userId;
                this.askBeforeRedirect = false;
                this._routerSvc.navigate(['/users', userId]);
            }, (error: any) => {
                console.log(error);
            });
    }

    private _disableAuthorizationControls(userType: string): void {
        const groupName: string = 'CUSTOMER' === userType ? 'customerAccRoles' : 'appRoles';
        const formArray: FormArray = <FormArray>this.usrFrm.controls[groupName];
        const formControls: FormControl[] = <FormControl[]>formArray.controls;
        for (let formControl of formControls) {
            if (!this._commonUtilSvc.isValidString(formControl.value)) {
                continue;
            }
            formControl.disable();
        }
    }
    private _createRoleGroups(): void {
        if ('new' === this.userId) {
            this.addNewAppRole({
                data: '',
                roles: []
            });
            this.addNewCustomerRole({
                data: '',
                roles: []
            });
        } else {
            let rolesGroups: any[] = this.userDetails.authorizations;
            let isEditable: boolean = false;
            /* if (this._addNewRoleGroup()) {
                isEditable = true;
                rolesGroups = [{
                    data: '',
                    roles: []
                }];
            } */
            rolesGroups.forEach((rolesGroup) => {
                'CUSTOMER' === this.userDetails.userType ? this.addNewCustomerRole(
                    rolesGroup,
                    isEditable
                ) : this.addNewAppRole(
                    rolesGroup,
                    isEditable
                );
            });
        }
    }
    private _createUserForm(): void {
        this._createRoleGroups();
        this.createFormControls();
        this.createFormGroup();
        this.renderForm = true;
        this.userType.valueChanges.subscribe((newValue: string) => {
            if ('CUSTOMER' === newValue) {
                this.userDetails.userType = 'CUSTOMER';
                this.usrFrm.controls.customerAccRoles.enable();
                this.usrFrm.controls.appRoles.disable();
                this.isRequired = true;
            } else {
                this.userDetails.userType = 'TENANT';
                this.isRequired = false;
                this.usrFrm.controls.appRoles.enable();
                this.usrFrm.controls.customerAccRoles.disable();
            }
            this._setValidators();
            this.usrFrm.controls.appRoles.updateValueAndValidity();
            this.usrFrm.controls.customerAccRoles.updateValueAndValidity();
            this._changeDetectRef.detectChanges();
        });
    }
    private _getUserDetails(): void {
        this._setRoles();
        this._userSvc.selectedItems = {};
        if ('new' === this.userId) {
            this._setActionButtons('new');
            this.showActionButtons = true;
            this.userDetails = {
                userType: 'CUSTOMER'
            };
            this._createUserForm();
            this.usrFrm.controls.appRoles.disable();
            return;
        } else {
            this._userSvc.getUser(this.userId)
                .subscribe((response) => {
                    this.userDetails = response;
                    if (this.userDetails.status === 'INACTIVE' ||
                        this.userDetails.status === 'REJECT'
                        || this.userDetails.userType === 'SHOPUSER') {
                        this.askBeforeRedirect = false;
                        this._redirectToUserDetails();
                        return;
                    }
                    this.isAccountLocked = this.userDetails.locked;
                    if ('CUSTOMER' === this.userDetails.userType) {
                        // in case of userType CUSTOMER, get authorised logged in users for the customers available
                        const customers: any[] = this._getAllAssociatedCustomer(this.userDetails.authorizations);
                        if (customers.length === 0 || this._authSvc.userHasPermissions('ApproveUser')) {
                            this.userDetails.authorizations.forEach((authorization) => {
                                authorization.isAuthorizedUser = true;
                            });
                            if (this.userDetails.status === 'APPROVAL_PENDING') {
                                this.showActionButtons = customers.length === 0 ? false : true;
                            } else {
                                this.showActionButtons = true;
                            }
                            this._commonMethodCall();
                        } else {
                            this._userSvc.checkBulkApprover(customers).subscribe(
                                (res) => {
                                    if (this._commonUtilSvc.isValidArray(res)) {
                                        this.userDetails.authorizations.forEach((authorization) => {
                                            authorization.isAuthorizedUser = -1 < res.indexOf(authorization.data);
                                        });
                                    }
                                    if (this.userDetails.status === 'APPROVAL_PENDING') {
                                        this.showActionButtons = this._commonUtilSvc.isValidArray(res) &&
                                            (-1 < res.indexOf(this.userDetails.authorizations[0].data)) ? true : false;
                                    } else {
                                        this.showActionButtons = true;
                                    }
                                    this._commonMethodCall();
                                },
                                () => {
                                    this.askBeforeRedirect = false;
                                    this._routerSvc.navigate(['/users', this.userId]);
                                }
                            );
                        }
                    } else {
                        this.showActionButtons = true;
                        this._commonMethodCall();
                    }
                });
        }
    }

    private _commonMethodCall(): void {
        this._setActionButtons(this.userDetails.status);
        this._disableAddApps();
        if ('CUSTOMER' === this.userDetails.userType) {
            this._setCustomerList();
        }
        this._setStateList(this.userDetails.country);
        this._createUserForm();
    }

    private _setActionButtons(status): void {
        this.actionButtons = [
            {
                disabled: false,
                text: 'APPROVAL_PENDING' === status ? 'COMMON.ACTIONS.REJECT' : 'COMMON.ACTIONS.CANCEL',
                btnClass: 'btn-default',
                action: 'APPROVAL_PENDING' === status ? 'rejectUser' : 'cancel',
                type: 'link',
                isPrimary: false,
                showDeactivate: status === 'ACTIVE' ? true : false,
                order: 1
            },
            {
                disabled: false,
                text: 'new' === status ? 'COMMON.ACTIONS.CREATE_USER' : 'APPROVAL_PENDING' === status ?
                    'COMMON.ACTIONS.APPROVE' : 'COMMON.ACTIONS.UPDATE_USER',
                action: 'new' === status ? 'createUser' : 'APPROVAL_PENDING' === status ?
                    'approveUser' : 'updateUser',
                btnClass: 'pzprimary',
                type: 'button',
                isPrimary: true,
                showDeactivate: status === 'ACTIVE' ? true : false,
                order: 2
            }
        ];
    }
    private _disableAddApps(): void {
        if (
            !this._commonUtilSvc.isValidArray(this.apps) ||
            !this._commonUtilSvc.isObject(this.userDetails)
        ) {
            return;
        }
        const rolesGroups: any[] = this.userDetails.authorizations;
        if (!this._commonUtilSvc.isValidArray(rolesGroups)) {
            return;
        }
        rolesGroups.forEach((rolesGroup) => {
            if (!this._commonUtilSvc.isObject(rolesGroup)) {
                return;
            }
            const item: any = this._commonUtilSvc.getFilteredItem(
                this.apps,
                'name',
                rolesGroup.data
            );
            if (this._commonUtilSvc.isObject(item)) {
                item.hideElement = true;
            }
        });
    }
    private _addNewRoleGroup(): boolean {
        const rolesGroups: any[] = this.userDetails.authorizations;
        if (
            'TENANT' === this.userDetails.userType
        ) {
            return !this._commonUtilSvc.isValidArray(rolesGroups);
        }
        const activeCustomers: any[] = this._commonUtilSvc.getFilteredItem(
            rolesGroups,
            'status',
            'ACTIVE',
            true
        );
        return !this._commonUtilSvc.isValidArray(activeCustomers);
    }
    private _setApps(): void {
        this._appSvc.getApplicationsList()
            .subscribe((apps: any[]) => {
                if (!this._commonUtilSvc.isValidArray(apps)) {
                    return;
                }
                this.apps = this._commonUtilSvc.getClonedArray(apps);
                this._disableAddApps();
            }, (error) => {
                this.apps = [];
                console.log('Could not fetch the list of applications.', error);
            });
    }
    private _setCityList(countrycode: string, stateCode: string): void {
        this._appSvc.getCityList(countrycode, stateCode).subscribe((response: any[]) => {
            this.cities = response;
        });
    }
    private _setCountryCodes(): void {
        this.countryCodes = this._commonUtilSvc.getUniqueObjects(this._appConfig._countryList, 'pCode');
    }
    private _setCountryList(): void {
        this._appSvc.getCountryList().subscribe((response: any[]) => {
            if (this._commonUtilSvc.isValidArray(response)) {
                this.countries = response;
            } else {
                // Populate countries from applicaiton config
                const countrySettings: any = this._appConfig.getAppFeatureSettings('countryList');
                const countries = this._commonUtilSvc.isValidArray(countrySettings.list) ?
                    countrySettings.list : [];
                const countryList = [];
                countries.forEach(country => {
                    countryList.push({
                        countryCode: country.code,
                        countryName: country.name
                    });
                });
                this.countries = countryList;
            }
        });
    }
    private _setCustomerList(): void {
        const authorizations: any[] = this.userDetails.authorizations;
        if (!this._commonUtilSvc.isValidArray(authorizations)) {
            return;
        }
        const custAccounts: any[] = [];
        authorizations.forEach((authorization: any) => {
            if ('INACTIVE' === authorization.status) {
                return;
            }
            const displayName: string = authorization.data + ': ' + authorization.displayName;
            custAccounts.push({
                code: displayName + ': ' + authorization.source,
                name: displayName,
                status: authorization.status,
                hideElement: true
            });
        });
        this.custAccounts = custAccounts;
    }
    private _setRoles(): void {
        const rolesInfo: any = this._appConfig.getAppFeatureSettings('rolesInfo') || {};
        this.roles.appRoles = rolesInfo['APPLICATION'];
        this.roles.custAccountRoles = this._commonUtilSvc.getClonedArray(rolesInfo['CUSTOMER']);
    }
    private _setSelectedCountry(countryCode: string): void {
        if (!this.country) {
            return;
        }
        this.country.setValue(countryCode);
        this._setStateList(countryCode);
    }
    private _setSelectedState(stateCode: string): void {
        if (!this.state) {
            return;
        }
        this.state.setValue(stateCode);
        this._setCityList(this.country.value, stateCode);
    }
    private _setStateList(countryCode: string): void {
        this._appSvc.getStateList(countryCode).subscribe((response: any[]) => {
            this.states = response;
        });
    }

    private _setValidators(): void {
        const requiredFields: string[] = [
            'address1',
            'state',
            'city',
            'companyName',
            'zip',
            'phoneno'
        ];
        for (let field of requiredFields) {
            const formField: FormControl = <FormControl>this[field];
            if (this.isRequired) {
                formField.setValidators(Validators.required);
            } else {
                formField.setValidators([]);
            }
            formField.updateValueAndValidity();
        }
    }
    validateEmailNotTaken(control: FormControl): Observable<any> {
        const emailId = control.value;
        return Observable.timer(500).switchMap(() => {
            return this._userSvc.checkEmailExistence(emailId)
                .map(
                    (response) => {
                        if (response && response.status === 'SUCCESS') {
                            return null;
                        } else {
                            return { emailTaken: true };
                        }
                    },
                    (error) => {
                        return { emailTaken: true };
                    }
                )
                .catch(err => Observable.of({ emailTaken: true }));
        });
    }

    canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
        // write proper logic here, if form is not submitted and there is a change in form then
        // prompt for comfirmation just before leaving this page
        // if (this.usrFrm && (this.usrFrm.dirty || this.usrFrm.touched) && !this.formSubmitted) {
        if (this.askBeforeRedirect) {
            return confirm('Do you want to discard the changes?');
        } else {
            return true;
        }
    }

    buttonClickedHandler(event) {
        const action: string = event.action;
        const actionKey: string = event.text;
        if (event.action === 'approveUser' || event.action === 'deactivateUser' || event.action === 'rejectUser') {
            let authorization: any = this._commonUtilSvc.isValidArray(this.userDetails.authorizations)
                ? this.userDetails.authorizations[0] : {};
            if (event.action === 'rejectUser' || event.action === 'deactivateUser') {
                event.roles = authorization.roles ? authorization.roles.slice() : [];
            } else {
                authorization = event.authorizations;
            }
            authorization.roles = event.roles.slice();
            event.authorizations = authorization;
            event.status = this.userDetails.status;
            event.customerNo = authorization ? authorization.data : null;
            event.customerName = authorization ? authorization.displayName : null;
        } else if (event.action === 'cancel') {
            this.askBeforeRedirect = true;
            if (this.userId === 'new') {
                this._routerSvc.navigate(['/users']);
            } else {
                this._routerSvc.navigate(['/users', this.userId]);
            }
            return;
        } else if (event.action === 'approveCustomer' && !this._commonUtilSvc.isValidArray(event.roles)) {
            this.formSubmitted = true;
            return;
        }
        this._showModal(event);
    }

    private _showModal(event: any): void {
        this.actionModal = this._modalService.show(ActionModalComponent, this._modalConfig);
        const userDetails: any = this.userDetails;
        const eventAction:string = event.action;
        const modalContent:any = {
            action: eventAction,
            actionKey: 'COMMON.ACTIONS.CONFIRM',
            roles: event.roles ? this._commonUtilSvc.getClonedArray(event.roles) : null,
            status: event.status,
            index: event.index,
            pos: event.pos,
            userType: userDetails.userType
        };

        if ('TENANT' === userDetails.userType) {
            modalContent.appCode = event.appCode;
            modalContent.appName = event.appName;
        } else {
            modalContent.customerNo = event.customerNo;
            modalContent.customerName = event.customerName;    
        }
        
        switch (eventAction) {
            case 'approveUser': {
                modalContent.titleKey = 'APPROVE_USER';
                modalContent.reasonReqd = false;
                break;
            }
            case 'rejectUser': {
                modalContent.titleKey = 'REJECT_USER';
                modalContent.reasonReqd = true;
                break;
            }
            case 'deactivateUser': {
                modalContent.titleKey = 'DEACTIVATE_USER';
                modalContent.reasonReqd = true;
                break;
            }
            case 'approveCustomer': {
                modalContent.titleKey = 'APPROVE_CUSTOMER';
                modalContent.reasonReqd = false;
                break;
            }
            case 'rejectCustomer': {
                modalContent.titleKey = 'REJECT_CUSTOMER';
                modalContent.reasonReqd = true;
                break;
            }
            case 'removeCustomer': {
                modalContent.titleKey = 'REMOVE_CUSTOMER';
                modalContent.reasonReqd = true;
                break;
            }
            default: 
            modalContent.titleKey = event.action;
            modalContent.reasonReqd = true;
            break;
        }
        
        modalContent.user = {
            userName: userDetails.firstName + ' ' + (userDetails.lastName ? userDetails.lastName : ''),
            emailId: userDetails.emailId,
            userId: userDetails.userId,
            comment: null,
            authorizations: event.authorizations
        };

        // Update modal content
        Object.assign(this.actionModal.content, modalContent);
    }

    onStatusChange() {
        const payload = {
            userId: this.userDetails.userId,
            lockFlag: !this.isAccountLocked
        };
        this._userSvc.updateLockStatus(payload).subscribe(
            (response) => {
                this.isAccountLocked = !this.isAccountLocked;
                this.userDetails.locked = this.isAccountLocked;
                this._eventEmitter
                    .onAlert
                    .emit({
                        type: 'alert-success',
                        msgKey: 'ALERT.LOCK_STATUS_CHANGED'
                    });
            },
            () => {
                // do not update locked status
            }
        );
    }

    ngOnDestroy() {
        this._actionModalSubscription.unsubscribe();
    }

    _getAllAssociatedCustomer(authorizations): any[] {
        const customers: any[] = [];
        if (
            !this._commonUtilSvc.isValidArray(authorizations)
        ) {
            return customers;
        }
        authorizations.forEach((authorization) => {
            customers.push({
                customerNumber: authorization.data,
                source: authorization.source
            });
        });
        return customers;
    }

    _redirectToUserDetails() {
        this._routerSvc.navigate(['/users', this.userId]);
    }

    toggleNotes() {
        if (!this.collapseCardData.card2.isCollapsed) {
            this.collapseCardData.card2.isCollapsed = true;
            return;
        }
        this._userSvc.getStatusLogs(this.userId).subscribe(
            (res: any) => {
                res = this._commonUtilSvc.isValidArray(res) ? res : [];
                this.collapseCardData.card2.isCollapsed = !this.collapseCardData.card2.isCollapsed;
                this.statusLogs = res.filter((item: any) => {
                    return this._commonUtilSvc.isValidString(item.comments);
                });
                this._commonUtilSvc.sortArrayObjects(this.statusLogs, 'modifiedOn', false, true);
            }
        );
    }

    private _updateAppRoles(event:any): void {
        const removedGroups:any[] = this.rolesGroup.appRoles.splice(event.index, 1);
        const appRoles: FormArray = this.usrFrm && <FormArray>this.usrFrm.controls['appRoles'];
        appRoles.removeAt(event.pos);
        this.rolesGroup.appRoles.forEach((roleGroup: any) => {
            if (roleGroup.pos < event.pos) {
                return;
            }
            roleGroup.pos = roleGroup.pos - 1;
        });
        
        // Remove disabled flag from previously added app
        const appCode:string = event.appCode;
        const selectedRoleGroup:any = removedGroups[0];
        const selectedGroup:any = this._commonUtilSvc.isObject(selectedRoleGroup) ? 
        selectedRoleGroup.group : {};

        const selectedApp:any = this._commonUtilSvc.getFilteredItem(
            this.apps,
            selectedGroup.selectCode,
            appCode
        );
        if (!this._commonUtilSvc.isObject(selectedApp)) {
            return;
        }
        delete selectedApp.disabled;
        delete selectedApp.selected;
    }

    private _updateCustomerAccRoles(event: any): void {
        this.rolesGroup.customerAccRoles.splice(event.index, 1);
        const customerAccRoles: FormArray = this.usrFrm && <FormArray>this.usrFrm.controls['customerAccRoles'];
        customerAccRoles.removeAt(event.pos);
        this.rolesGroup.customerAccRoles.forEach((roleGroup: any) => {
            if (roleGroup.pos < event.pos) {
                return;
            }
            roleGroup.pos = roleGroup.pos - 1;
        });
        /* if (!this._commonUtilSvc.isValidArray(customerAccRoles.controls)) {
            this.addNewCustomerRole({
                data: '',
                roles: []
            });
        } */
    }
}
