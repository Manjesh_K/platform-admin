import { FormControl, FormArray } from "@angular/forms";

/* interface IUser {
    userId?: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    emailId?: string;
    address1?: string;
    address2?: string;
    street?: string;
    city?: string;
    state?: string;
    country?: string;
    zip?: string;
    phoneno?: string;
    mobile?: string;
    fax?: string;
    phonetype?: string;
    source?: string;
    companyName?: string;
    userType?: string;
    mCountry?: string;
    locale?: string;
    authorizations?: any[];
    status?: string;
    locked?: boolean;
}; */

export class User {
    constructor(
        public appRoles?: FormArray,
        public customerAccRoles?: FormArray,
        public username?: FormControl,
        public emailId?: FormControl,
        public firstName?: FormControl,
        public lastName?: FormControl,
        public address1?: FormControl,
        public address2?: FormControl,
        public companyName?: FormControl,
        public zip?: FormControl,
        public country?: FormControl,
        public state?: FormControl,
        public city?: FormControl,
        public mobile?: FormControl,
        public phoneno?: FormControl,
        public fax?: FormControl,
        public preferredContactType?: FormControl,
        public userType?: FormControl,
        public mCountry?: FormControl,
        public locale?: FormControl,
        public mobileCd?: FormControl,
        public phonenoCd?: FormControl,
        public faxCd?: FormControl,
        public status?: FormControl

    ) {
    }
}
