/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Manjesh
 */
import {
    Component,
    Input,
    OnDestroy,
    TemplateRef
} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { SubscriptionHistory } from '../../core/subscriptionHistory.helper';
import { EventEmitterService } from '../../core/event-emitter.service';
import { UsersService } from '../users.service';
import { AppConfig } from '../../config/app.config';
@Component({
    selector: 'pz-reset-password',
    template: `
        <div class="d-inline-block" style="cursor: pointer" *ngIf="user.status === 'ACTIVE' && user.emailId">
            <a class="text-primary"
            (click)="confirmResetPassword(template)">{{'COMMON.ACTIONS.RESET_PSWRD' | translate}}</a>
            <i class="fa fa-question-circle txt-gray-75" aria-hidden="true"></i>
        </div>
        <ng-template #template>
            <div class="modal-body">
                <h4 class="modal-title font-weight-normal mb-4 lh-1 txt-gray-75">{{'RESET_PASSWORD.TITLE' | translate}}</h4>
                <div class="form-group mb-2">
                    <label class="mb-0">{{'USER_FORM.FIELDS.FULL_NAME.LABEL' | translate}}</label>
                    <p class="mb-0 txt-readonly-form-data">{{user.firstName+' '+(user.lastName ? user.lastName : "")}}</p>
                </div>
                <div class="form-group mb-4">
                    <label class="mb-0">{{(includeUsername ?
                        'USER_FORM.FIELDS.USER_NAME.LABEL' : 'USER_FORM.FIELDS.EMAIL.LABEL') | translate}}</label>
                    <p class="mb-0 txt-readonly-form-data">{{user.userId}}</p>
                </div>
                <div class="text-right">
                    <button class="btn btn-md btn-link text-capitalize txt-gray-7 fs-14 font-weight-semi-bold p-1"
                    type="button" (click)="decline()">{{'COMMON.ACTIONS.CANCEL' | translate}}</button>
                    <button class="btn btn-md btn-pzprimary text-uppercase" type="button"
                    (click)="confirm()">{{'COMMON.ACTIONS.CONFIRM' | translate}}</button>
                </div>
            </div>
        </ng-template>
    `,
    styles: [`
        a {
            font-size: 13px;
            font-weight: 600;
        }
    `]
})
export class ResetPasswordComponent extends SubscriptionHistory implements OnDestroy {
    public declineKey: string;
    public confirmKey: string;
    public titleKey: string;
    public confirmModalRef: BsModalRef;
    public includeUsername: boolean;
    @Input() public user: any;
    private _modalConfig = {
        animated: true,
        class: 'pz-modal-popup action-modal',
        keyboard: false,
        backdrop: true,
        ignoreBackdropClick: true
    };
    constructor(
        private _appConfig: AppConfig,
        private _eventEmitter: EventEmitterService,
        private _userSvc: UsersService,
        private _modalService: BsModalService
    ) {
        super();
        const userIdConfig = _appConfig.getAppFeatureSettings(
            'userIdConfiguration'
        ) || {};
        this.includeUsername = userIdConfig.isCustomized;
        this.subscriptions.push(
            _eventEmitter.onNavigationSuccess.subscribe(
                () => this.closePopover()
            )
        );
    }
    confirmResetPassword(template: TemplateRef<any>): void {
        this.confirmModalRef = this._modalService.show(template, Object.assign(this._modalConfig));
    }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public closePopover() {
        this.confirmModalRef.hide();
    }
    decline(): void {
        this.closePopover();
    }
    confirm(): void {
        const queryParams: any = {
            emailId: this.user.emailId
        };
        this._userSvc.resetPassword(queryParams).subscribe(
            (res) => {
                const notMsgKey = 'ALERT.PSWRD_SENT';
                if (res.errorCode) {
                    this._notifyMsg('alert-warning', 'ALERT.SERVER_ERR', {});
                    return;
                }
                this._notifyMsg('alert-success', notMsgKey, { 'email': this.user.emailId });
                this.closePopover();
            }, () => {
                this._notifyMsg('alert-warning', 'ALERT.SERVER_ERR', {});
            }
        );
    }
    private _notifyMsg(_type: string, _msgKey: string, _params: any) {
        this._eventEmitter
            .onAlert
            .emit({
                type: _type,
                msgKey: _msgKey,
                params: _params
            });
    }
}
