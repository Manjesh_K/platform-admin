import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { User } from './user.model';
import { HttpClient, HttpRequest, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ApiDispatcherService } from '../core/api-dispatcher.service';
import { CommonUtilService } from '../core/common-util.service';
import { unescapeIdentifier } from '@angular/compiler';


@Injectable()
export class UsersService {
    public isGridView: boolean = true;
    public usersChanged = new Subject();
    public filtesReset = new Subject();
    public actionModalActionComplete = new Subject<any>();
    public selectedItems: any;
    constructor(private http: HttpClient,
        private _apiDispatcher: ApiDispatcherService,
        private _commonUtilSvc: CommonUtilService
    ) { }
    public createUser(userDetails: any): Observable<any> {
        return this._apiDispatcher.doPostApiCall(
            'createUser',
            userDetails
        );
    }
    public getRequestPayLoad(formDetails: any, userType: string): any {
        if (!this._commonUtilSvc.isValidObject(formDetails)) {
            return {};
        }
        const groupName: string = 'CUSTOMER' === userType ? 'customerAccRoles' : 'appRoles';
        const requestPayLoad: any = {
            authorizations: []
        };
        const countryCodes: string[] = [
            'mobileCd',
            'phonenoCd',
            'faxCd'
        ];
        for (const prop in formDetails) {
            if (!formDetails.hasOwnProperty(prop) || -1 < countryCodes.indexOf(prop)) {
                continue;
            }
            if (prop === groupName) {
                const roleGroups: any[] = formDetails[groupName];
                roleGroups.forEach((roleGroup: any) => {
                    const group: string = roleGroup.group;
                    if (!this._commonUtilSvc.isValidString(group)) {
                        return;
                    }
                    const groupData: any = {
                        'roles': roleGroup.roles,
                        'type': 'CUSTOMER' === userType ? 'CUSTOMER' : 'APPLICATION',
                        'source': null
                    };

                    if (groupName === 'customerAccRoles') {
                        const data = group.split(': ');
                        groupData.data = data[0];
                        groupData.displayName = data[1];
                        groupData.source = data[2];
                    } else {
                        groupData.data = group;
                    }
                    requestPayLoad.authorizations.push(groupData);
                });
            } else {
                requestPayLoad[prop] = formDetails[prop];
            }
        }
        return requestPayLoad;
    }
    getUsersCount(): Observable<any> {
        return this._apiDispatcher.doGetApiCall('usersCountByStatus');
    }
    getUsers(queryParams: any): Observable<any> {
        return this._apiDispatcher.doGetApiCall(`getUsers`, undefined, queryParams, false);
    }
    getUser(userId: string): Observable<any> {
        const routeParams: any = {
            id: userId
        };
        return this._apiDispatcher.doGetApiCall(`getUser`, routeParams, undefined, false);
    }
    updateLockStatus(payload: any): Observable<any> {
        return this._apiDispatcher.doPostApiCall(`updateLock`, payload, undefined, undefined);
    }
    /**
     * @desc: approveUser api is used to approve a user
     */
    approveUser(routeParams: any, payload: any): Observable<any> {
        return this._apiDispatcher.doPostApiCall(`approveUser`, payload, routeParams, undefined);
    }
    /**
     * @desc: rejectUser api is used to reject a user
     */
    rejectUser(routeParams: any, payload: any): Observable<any> {
        return this._apiDispatcher.doPostApiCall(`rejetUser`, payload, routeParams, undefined);
    }
    /**
     * @desc: deactivateUser api is used to remove or deactivate a user
     */
    deactivateUser(routeParams: any, payload: any): Observable<any> {
        return this._apiDispatcher.doPostApiCall(`deactivateUser`, payload, routeParams, undefined);
    }
    /**
     * @desc: manageCustomer api is used to approve, reject or remove customer associated with a user
     */
    manageCustomer(routeParams: any, payload: any): Observable<any> {
        return this._apiDispatcher.doPostApiCall(`manageCustomer`, payload, routeParams, undefined);
    }
    removeAppRoles(uId: string, applicationName: string, reason: string): Observable<any> {
        return this._apiDispatcher.doPostApiCall(
            'removeAppRoles',
            {
                comment: reason,
                status: 'INACTIVE'
            },
            {
                userId: uId,
                appName: applicationName
            }
        );
    }
    /**
     * @desc: send password link to reset password for the user
     */
    resetPassword(queryParams: any): Observable<any> {
        return this._apiDispatcher.doGetApiCall(`resetPassword`, undefined, queryParams);
    }
    saveUser(userDetails: any, userId: string) {
        if ('new' === userId) {
            return this.createUser(userDetails);
        } else {
            return this.updateUser(userDetails, userId);
        }
    }
    updateUser(userDetails: any, userId: string): Observable<any> {
        return this._apiDispatcher.doPutApiCall(
            'updateUser',
            userDetails,
            {
                id: userId
            }
        );
    }

    checkEmailExistence(email: string): Observable<any> {
        const queryParams: any = {
            emailId: email
        };
        return this._apiDispatcher.doGetApiCall(`checkEmailExistence`, undefined, queryParams, false);
    }

    /**
     * @desc: checkBulkApprover api is used to get customer list of approver
     */
    checkBulkApprover(payload: any): Observable<any> {
        return this._apiDispatcher.doPostApiCall(`bulkApproverCheck`, payload, undefined, undefined);
    }

    /**
     * @desc: get status logs for user
     */
    getStatusLogs(userId: string): Observable<any> {
        const params: any = {
            'userId': userId
        };
        return this._apiDispatcher.doGetApiCall(`statusLogs`, params, undefined, false);
    }
}
