import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { PaginationModule } from 'ngx-bootstrap/pagination';

import { UserDetailComponent } from './user-detail/user-detail.component';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import { UsersService } from './users.service';
import { RolesByAppFilterPipe } from '../core/rolesByAppFilter.pipe';
import { UserItemComponent } from './user-item/user-item.component';
import { SideMenuComponent } from '../components/side-menu/side-menu.component';
import { RolesByCustomerFilterPipe } from '../core/rolesByCustomerFilter.pipe';
import { SaveUserComponent } from './save-user/save-user.component';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ComponentsModule } from '../components/components.module';
import { RolesGroupComponent } from './roles-group/roles-group.component';
import { RolesCardComponent } from './roles-card/roles-card.component';
import { NavbarFixedBottomComponent } from '../components/navbar-fixed-bottom/navbar-fixed-bottom.component';
import { ActionModalComponent } from './action-modal/action-modal.component';
import { ModalModule } from 'ngx-bootstrap/modal/modal.module';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { CutomerListPopoverComponent } from './cutomer-list-popover/cutomer-list-popover.component';
import { PopoverModule } from 'ngx-bootstrap/popover/popover.module';
import { RoleFilterPipe } from '../core/role-filter.pipe';

@NgModule({
    declarations: [
        ActionModalComponent,
        UsersComponent,
        UserDetailComponent,
        UserDetailComponent,
        RolesByAppFilterPipe,
        RolesByCustomerFilterPipe,
        UserItemComponent,
        SideMenuComponent,
        SaveUserComponent,
        RolesGroupComponent,
        RolesCardComponent,
        NavbarFixedBottomComponent,
        ResetPasswordComponent,
        CutomerListPopoverComponent,
        RoleFilterPipe
    ],
    entryComponents: [
        ActionModalComponent
    ],
    imports: [
        CommonModule,
        ComponentsModule,
        UsersRoutingModule,
        TranslateModule,
        ReactiveFormsModule,
        FormsModule,
        BsDropdownModule.forRoot(),
        CollapseModule.forRoot(),
        TypeaheadModule.forRoot(),
        PaginationModule.forRoot(),
        ModalModule.forRoot(),
        PopoverModule.forRoot()
    ]
})
export class UsersModule {

}
