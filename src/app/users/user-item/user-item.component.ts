import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { User } from '../user.model';
import { UsersService } from '../users.service';

@Component({
  selector: 'pz-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.scss']
})
export class UserItemComponent implements OnInit, OnDestroy {
  @Input() user: any;
  @Input() index: number;
  @Output() clickedUser = new EventEmitter<any>();
  constructor(
    private usersService: UsersService,
    private router: Router,
    private route: ActivatedRoute
  ) { }
  ngOnInit() {
  }
  showUserDetails() {
    this.clickedUser.emit(this.user);
  }
  ngOnDestroy() {
    this.clickedUser.unsubscribe();
  }

}
