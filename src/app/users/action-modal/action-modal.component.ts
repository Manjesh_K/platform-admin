/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Component,
    OnDestroy,
    ViewChild,
    OnInit
} from '@angular/core';
import {
    NgForm
} from '@angular/forms';
import { SubscriptionHistory } from '../../core/subscriptionHistory.helper';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CommonUtilService } from '../../core/common-util.service';
import { EventEmitterService } from '../../core/event-emitter.service';
import { UsersService } from '../users.service';
@Component({
    selector: 'pz-action-modal',
    template: `
    <div class="modal-body">
        <h4 class="modal-title font-weight-normal mb-4 lh-1 txt-gray-75">{{'ACTION_MODAL.TITLE.' + titleKey | translate}}</h4>
        <form class="action-modal-form" (ngSubmit)="onSubmit()" #actionFrm="ngForm" role="form">
            <div class="form-group mb-2">
                <label class="mb-0">{{'USER_FORM.FIELDS.FULL_NAME.LABEL' | translate}}</label>
                <p class="mb-0 txt-readonly-form-data text-uppercase">{{user.userName}}</p>
            </div>
            <div class="form-group mb-2">
                <label class="mb-0">{{'USER_FORM.FIELDS.EMAIL.LABEL' | translate}}</label>
                <p class="mb-0 txt-readonly-form-data">{{user.userId}}</p>
            </div>
            <div class="form-group mb-2" *ngIf="action !== 'deactivateUser'">
                <label class="mb-0" translate="{{'TENANT' === userType ? 'USER_FORM.FIELDS.APP_NAME.LABEL_READONLY' : 'USER_FORM.FIELDS.CUST_ACC.LABEL_READONLY'}}"></label>
                <p class="mb-0 txt-readonly-form-data" *ngIf="customerName">{{customerNo}}
                 ({{customerName}})</p>
                <p class="mb-0 txt-readonly-form-data" *ngIf="appName">{{appName}}</p>
            </div>
            <div class="form-group mb-2">
                <label class="mb-0">Add Reason<span *ngIf="reasonReqd" class="text-muted">*</span></label>
                <textarea class="form-control pilm-form-control" maxlength="250" [required]="reasonReqd"
                name="comments" id="comments" [(ngModel)]="reason" #comments="ngModel"></textarea>
                <p class="text-danger fs-12 mb-2"
                [ngClass]="{'visible': comments.invalid && (comments.dirty || comments.comments),
                'invisible': !(comments.invalid && (comments.dirty || comments.touched))}"
                translate="COMMON.ERRORS.FIELD_REQ"></p>
            </div>
            <div class="text-right">
                <button class="btn btn-md btn-link text-capitalize txt-gray-7 fs-14 font-weight-semi-bold p-1"
                type="button" (click)="closePopover()">{{'COMMON.ACTIONS.CANCEL' | translate}}</button>
                <button class="btn btn-md btn-pzprimary text-uppercase" type="submit">{{actionKey | translate}}</button>
            </div>
        </form>
    </div>
    `
})
export class ActionModalComponent extends SubscriptionHistory implements OnInit, OnDestroy {
    public reason: string;
    public user: any = {};
    public actionKey: string;
    public action: string;
    public reasonReqd: boolean;
    public titleKey: string;
    public roles: any;
    public appCode: string;
    public appName: string;
    public customerNo: string;
    public customerName: string;
    public status: string;
    public index: number;
    public pos: number;
    public userType: string;
    @ViewChild('actionFrm')
    public actionFrm: NgForm;
    constructor(
        public bsModalRef: BsModalRef,
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _userSvc: UsersService
    ) {
        super();
        this.subscriptions.push(
            _eventEmitter.onNavigationSuccess.subscribe(
                () => this.closePopover()
            )
        );
    }
    public ngOnInit() { }
    public ngOnDestroy() {
        this.unsubscribe();
    }
    public closePopover() {
        this.bsModalRef.hide();
    }
    public onSubmit() {
        if (this.actionFrm.invalid) {
            this._commonUtil.setFormDirty(this.actionFrm.controls);
            return;
        }
        if (this.action === 'approveUser') {
            this._userSvc.approveUser(this._getManageUserParams(), this._getManageUserPayload(this.action)).subscribe(
                (res) => {
                    const notMsgKey = 'ALERT.USER_APPROVED';
                    this._notifyMsg('alert-success', notMsgKey);
                    this.closePopover();
                    this._userSvc.actionModalActionComplete.next({
                        action: this.action
                    });
                }, () => {
                    this._notifyMsg('alert-warning', 'ALERT.SERVER_ERR');
                }
            );
        } else if (this.action === 'rejectUser') {
            this._userSvc.rejectUser(this._getManageUserParams(), this._getManageUserPayload(this.action)).subscribe(
                (res) => {
                    const notMsgKey = 'ALERT.USER_REJECTED';
                    this._notifyMsg('alert-success', notMsgKey);
                    this.closePopover();
                    this._userSvc.actionModalActionComplete.next({
                        action: this.action
                    });
                }, () => {
                    this._notifyMsg('alert-warning', 'ALERT.SERVER_ERR');
                }
            );
        } else if (this.action === 'deactivateUser') {
            this._userSvc.deactivateUser(this._getManageUserParams(), this._getManageUserPayload(this.action)).subscribe(
                (res) => {
                    const notMsgKey = 'ALERT.USER_DEACTIVATED';
                    this._notifyMsg('alert-success', notMsgKey);
                    this.closePopover();
                    this._userSvc.actionModalActionComplete.next({
                        action: this.action
                    });
                }, () => {
                    this._notifyMsg('alert-warning', 'ALERT.SERVER_ERR');
                }
            );
        } else if (this.action === 'approveCustomer' || this.action === 'rejectCustomer' || this.action === 'removeCustomer') {
            const params: any = {
                'userId': this.user.userId,
                'cutomerNo': this.customerNo
            };
            const status = this.action === 'approveCustomer' ? 'ACTIVE' :
                (this.action === 'rejectCustomer' ? 'REJECT' : 'INACTIVE');
            const payload: any = {
                'status': status,
                'roles': this.roles,
                'comment': this.reason
            };
            this._userSvc.manageCustomer(params, payload).subscribe(
                (res) => {
                    const notMsgKey = this.action === 'approveCustomer' ? 'ALERT.CUSTOMER_APPROVED' :
                        (this.action === 'rejectCustomer' ? 'ALERT.CUSTOMER_REJECTED' : 'ALERT.CUSTOMER_REMOVED');
                    this._notifyMsg('alert-success', notMsgKey);
                    this.closePopover();
                    this._userSvc.actionModalActionComplete.next({
                        action: this.action,
                        index: this.index,
                        pos: this.pos
                    });
                }, () => {
                    this._notifyMsg('alert-warning', 'ALERT.SERVER_ERR');
                }
            );
        } else if ('REMOVE_APP_ROLES' === this.action) {
            this._removeAppRoles();
        }
    }

    private _removeAppRoles() {
        if (!this._commonUtil.isValidString(this.status)) {
            this._onActionComplete();
            return;
        }

        this._userSvc.removeAppRoles(
            this.user.userId,
            this.appCode,
            this.reason
        ).subscribe(() => {
            this._notifyMsg('alert-success', 'ALERT.APP_ROLES_REMOVED');
            this._onActionComplete();
            this.closePopover();
        }, () => {
            this._notifyMsg('alert-warning', 'ALERT.SERVER_ERR');
        });
    }

    private _onActionComplete(): void {
        this._userSvc.actionModalActionComplete.next({
            action: this.action,
            index: this.index,
            pos: this.pos,
            appCode: this.appCode
        });
    }

    private _getManageUserPayload(action) {
        return {
            'status': action === 'deactivateUser' ? 'INACTIVE' : '',
            'comment': this.reason,
            'authorizations': [this.user.authorizations]
        };
    }

    private _getManageUserParams() {
        return {
            'userId': this.user.userId
        };
    }

    private _notifyMsg(_type: string, _msgKey: string) {
        this._eventEmitter
            .onAlert
            .emit({
                type: _type,
                msgKey: _msgKey
            });
    }
}
