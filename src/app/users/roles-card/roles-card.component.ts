import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';

@Component({
    selector: 'pz-roles-card',
    templateUrl: './roles-card.component.html',
    styleUrls: ['./roles-card.component.scss']
})
export class RolesCardComponent {
    @Input()
    public cardTitleLable: string;
    @Input()
    public addRoleGroupLabel: string;
    @Input()
    public addRoleGroup: boolean = false;
    @Input()
    public fGroup: FormGroup;
    @Input()
    public roleGroups: any[];
    @Input()
    public groupItems: any[];
    @Input()
    public formSubmitted: boolean = false;
    @Input()
    public roleGroupType: string; // 'APP_ROLE'
    @Output()
    public addNewRoleGroup: EventEmitter<any> = new EventEmitter();
    @Output()
    public removeRoleGroup: EventEmitter<any> = new EventEmitter();
    public rgCard: any = {
        isCollapsed: false
    };
    @Input()
    public formArray: FormArray;
    constructor() { }
    public addNew(): void {
        this.addNewRoleGroup.emit({
            data: '',
            roles: []
        });
    }

    public removeGroup(event:any): void {
        this.removeRoleGroup.emit(event);
    }
}
