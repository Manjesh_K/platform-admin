import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Subject } from 'rxjs/Subject';
import { debounceTime } from 'rxjs/operators/debounceTime';
import { switchMap } from 'rxjs/operators/switchMap';
import { Observable } from 'rxjs/Observable';
import { distinctUntilChanged } from 'rxjs/operators/distinctUntilChanged';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { CommonUtilService } from '../core/common-util.service';

@Component({
  selector: 'pz-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({
        transform: 'translate3d(100%, 0px, 0px)'
      })),
      state('out', style({
        transform: 'translate3d(0px, 0px, 0px)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
  ]
})
export class UsersComponent implements OnInit, OnDestroy {
  public users = [];
  usersSubscription: Subscription;
  public isGridView: boolean;
  public isSorted: boolean;
  public menuState: string = 'in';
  public maxSize = 10;
  public currentPage: number = 1;
  public pageSize: number = 9; // should come from app config api eresponse
  public totalElements: number;
  public searchForm = new FormGroup({
    searchUserText: new FormControl(null)
  });
  constructor(
    private commonUtilSvc: CommonUtilService,
    private usersService: UsersService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.maxSize = window.innerWidth > 767 ? 10 : 5;
    this.isGridView = this.usersService.isGridView;
    this.route.queryParams.subscribe(
      (queryParams: Params) => {
        this.setSearchForm(queryParams.pattern);
        this.getUserList(queryParams);
      }
    );

    this.usersService.usersChanged.pipe(
      debounceTime(500),
      distinctUntilChanged()
    ).subscribe((searchPattern: any) => {
      searchPattern = searchPattern ? searchPattern : null;
      this.router.navigate([], {
        queryParams: {
          'pattern': searchPattern,
          'page': 1,
          'size': this.pageSize,
          'sortBY': 'firstName',
          'sortOrder': 'ASC'
        }, queryParamsHandling: 'merge'
      });
    });
  }

  getUserList(params: any) {
    const queryParams: any = Object.assign({}, params);
    if (!this.commonUtilSvc.isValidString(queryParams['page'])) {
      queryParams.page = 1;
    }
    queryParams.size = this.pageSize;
    if (this.commonUtilSvc.isValidString(queryParams['sortOrder']) && queryParams['sortOrder'] === 'DESC') {
      this.isSorted = false;
    } else {
      this.isSorted = true;
      queryParams.sortOrder = 'ASC';
    }
    queryParams.sortBY = 'firstName|lastName';
    this.totalElements = 1; // making pagination to reacreate
    this.usersService.getUsers(queryParams).subscribe(
      (response: any) => {
        if (response) {
          const resBody: any = response.body;
          this.users = response.elements;
          this.currentPage = response.pageable.pageNumber;
          this.totalElements = response.totalElements;
        } else {
          // show error message, saying there is no data available
        }
      }
    );
  }

  search(searchText: string, clearText) {
    if (clearText) {
      this.setSearchForm(null);
    }
    this.usersService.usersChanged.next(searchText);
  }

  setSearchForm(pattern: any) {
    pattern = pattern ? pattern : null;
    this.searchForm.setValue({
      'searchUserText': pattern
    });
  }

  createUser() {
    this.router.navigate(['new'], { relativeTo: this.route });
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.router.navigate([],
      {
        queryParams: {
          'page': event.page,
          'size': event.itemsPerPage
        },
        queryParamsHandling: 'merge'
      });
  }

  toggleMenu() {
    this.menuState = this.menuState === 'out' ? 'in' : 'out';
  }

  closeMenu(evemt) {
    this.menuState = 'in';
  }

  toggleView() {
    this.isGridView = !this.isGridView;
    this.usersService.isGridView = this.isGridView;
  }

  onClickUser(user: any) {
    this.router.navigate([user.userId], { relativeTo: this.route });
  }

  sortByName() {
    this.isSorted = !this.isSorted;
    const params: any = {
      'sortBY': 'firstName|lastName'
    };
    if ( this.isSorted) {
      params.sortOrder = 'ASC';
    } else {
      params.sortOrder = 'DESC';
    }
    this.router.navigate([],
      {
        queryParams: params,
        queryParamsHandling: 'merge'
      });
  }

  ngOnDestroy() {
    //this.usersSubscription.unsubscribe();
  }

}
