import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { UsersService } from '../users.service';
import { CommonUtilService } from '../../core/common-util.service';
import { CanComponentDeactivate } from '../../core/can-deactivate-guard.service';
import { Observable } from 'rxjs/Observable';
import { ActionModalComponent } from '../action-modal/action-modal.component';
import { AuthService } from '../../auth/auth.service';
import { EventEmitterService } from '../../core/event-emitter.service';
import { AppConfig } from '../../config/app.config';
import { NavigationService } from '../../core/navigation.service';
import { User } from '../user.model';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { AppService } from '../../core/app.service';

@Component({
    selector: 'pz-user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent extends User implements OnInit, OnDestroy, CanComponentDeactivate {
    public actionModal: BsModalRef;
    public confirmModal: BsModalRef;
    private _actionModalSubscription: Subscription;
    public actionButtons: any[];
    public editPermission: string = 'EditUser';
    public user: any;
    public enableRolesEdit: boolean;
    public hasEditPermission: boolean;
    public hasApprovePermission: boolean;
    public isAccountLocked: boolean;
    public isRolesEditable: boolean;
    public renderForm: boolean = false;
    public roles: any = {};
    public statusLogs: any[] = [];
    public approveUserPermission: string = 'ApproveUser';
    public rolesGroup: any = {
        appRoles: [

        ],
        customerAccRoles: [

        ]
    };
    public apps: any[] = [];
    public custAccounts: any[] = [];
    public formSubmitted: boolean = false;
    public usrFrm: FormGroup;
    public contactType: any;
    public modes: any[] = [
        {
            name: 'Mobile',
            code: 'Mobile Phone'
        },
        {
            name: 'Email',
            code: 'Email'
        },
        {
            name: 'Phone',
            code: 'Work Phone'
        }
    ];
    public collapseCardData: any = {
        card1: {
            isCollapsed: true
        },
        card2: {
            isCollapsed: true
        },
        card3: {
            isCollapsed: true
        },
        card4: {
            isCollapsed: true
        },
        card5: {
            isCollapsed: true
        }
    };
    private _authorizedCustomerNumbers: any[] = [];
    private _modalConfig = {
        animated: true,
        class: 'pz-modal-popup action-modal',
        keyboard: false,
        backdrop: true,
        ignoreBackdropClick: true
    };
    constructor(
        private _authSvc: AuthService,
        private _appSvc: AppService,
        private commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _modalService: BsModalService,
        private route: ActivatedRoute,
        private router: Router,
        private userService: UsersService,
        private _appConfig: AppConfig,
        private _routerSvc: NavigationService
    ) {
        super();
        this._setRoles();
        this._setApps();
    }

    ngOnInit() {
        this.hasApprovePermission = this._authSvc.userHasPermissions('ApproveUser');
        this.hasEditPermission = this._authSvc.userHasPermissions('EditUser');
        this.enableRolesEdit = false;
        this.userService.selectedItems = {};
        this.route.params.subscribe(
            (params: Params) => {
                const userId = params['userId'];
                this.getUserDetails(userId);
            }
        );
        this._actionModalSubscription = this.userService.actionModalActionComplete.subscribe(
            (actionData: any) => {
                if (actionData.action === 'approveCustomer' || actionData.action === 'rejectCustomer') {
                    this.updateCustomerStatus(actionData);
                } else if (
                    actionData.action === 'deactivateUser' ||
                    actionData.action === 'approveUser' ||
                    actionData.action === 'rejectUser'
                ) {
                    this.updateUserStatus(actionData);
                }
            }
        );
    }

    updateUserRoles() {
        this.commonUtil.delayCallback('updateUserRoles', () => {
            if (this.usrFrm.invalid) {
                this.formSubmitted = true;
                return;
            }
            this.formSubmitted = false;
            if (this.enableRolesEdit) {
                this._updateAssociatedCustomerRoles();
                return;
            }
            this._doApproveUser();
        }, 100);
    }

    private _doApproveUser() {
        const userStatus: string = this.user.status;
        if ('APPROVAL_PENDING' !== userStatus) {
            return;
        }

        const userType: string = this.user.userType;
        const requestPayload: any = this.userService.getRequestPayLoad(this.usrFrm.value, userType);
        const authorizations: any[] = requestPayload.authorizations;

        if (!this.commonUtil.isValidArray(authorizations)) {
            return;
        }

        const event: any = {
            'action': 'approveUser',
            'text': 'APPROVE',
            'roles': authorizations[0].roles,
            'status': userStatus
        };
        this.buttonClickedHandler(event);
    }

    updateCustomerStatus(event) {
        const status = event.action === 'approveCustomer' ? 'ACTIVE' : 'REJECT';
        const roleGroup: any = this.rolesGroup.customerAccRoles[event.index];

        if (!this.commonUtil.isObject(roleGroup)) {
            return;
        }
        // Update role group status
        roleGroup.actions.status = status;
        roleGroup.actions.status = status;
        roleGroup.role.editable = false;
    }

    updateUserStatus(event) {
        const status = event.action === 'approveUser' ? 'ACTIVE' : event.action === 'rejectUser' ? 'REJECT' : 'INACTIVE';
        this.user.status = status;
        this.editPermission = status === 'ACTIVE' ? this.editPermission : 'NoEditUser';
        if ('CUSTOMER' === this.user.userType) {
            // Enable roles edit, post user profile approval
            this._doEnableRolesEdit(status);
            this._setActionButtons(status);
        }
        const roleGroup: any = this.rolesGroup.customerAccRoles[0];
        if (this.commonUtil.isObject(roleGroup)) {
            roleGroup.actions.status = status;
            roleGroup.role.editable = false;
        }
    }

    public actionCallbackForCustomer(event) {
        this.buttonClickedHandler(event);
    }

    public actionCallbackForApps() { }


    private _doEnableRolesEdit(userProfileStatus: string): void {
        this.enableRolesEdit = (
            this._authSvc.userHasRole('PZV_SALES') &&
            'ACTIVE' === userProfileStatus
        ) ? !this.hasEditPermission : false;
    }

    private _setRoles(): void {
        const rolesInfo: any = this._appConfig.getAppFeatureSettings('rolesInfo') || {};
        this.roles.appRoles = rolesInfo['APPLICATION'];
        this.roles.custAccountRoles = this.commonUtil.getClonedArray(rolesInfo['CUSTOMER']);
    }

    private _setApps(): void {
        this._appSvc.getApplicationsList()
            .subscribe((apps: any[]) => {
                if (!this.commonUtil.isValidArray(apps)) {
                    return;
                }
                this.apps = this.commonUtil.getClonedArray(apps);
                this._disableAddApps();
            }, (error) => {
                this.apps = [];
                console.log('Could not fetch the list of applications.', error);
            });
    }

    editUser() {
        if (this.enableRolesEdit) {
            this.isRolesEditable = true;
            this.markRolesForEdit();
        } else {
            this.router.navigate(['save'], { relativeTo: this.route });
        }
    }

    markRolesForEdit(action?: string) {
        const roleGroups: any[] = this.rolesGroup.customerAccRoles;
        if (!this.commonUtil.isValidArray(roleGroups)) {
            return;
        }

        const customerAccRoles: FormArray = this.usrFrm && <FormArray>this.usrFrm.controls['customerAccRoles'];

        for (let i = 0; i < roleGroups.length; ++i) {
            if (!this.commonUtil.isObject(roleGroups[i])) {
                continue;
            }
            const roleGroup: any = roleGroups[i];
            const groupActions: any = roleGroup.actions;
            const roleCardPos: number = isNaN(roleGroup.pos) ? -1 : roleGroup.pos;
            const group: any = roleGroup.group || {};
            const role: any = roleGroup.role || {};
            if (group.editable) {
                // If user cancels to save the newly added group roles
                if ('cancel' === action) {
                    roleGroups.splice(i, 1);
                    if (-1 !== roleCardPos) {
                        customerAccRoles.removeAt(roleCardPos);
                        this._removeCachedItem(group.control.value);
                    }
                    --i;
                    continue;
                }
                // If newly added group is being saved
                group.editable = false;
            }
            if (!groupActions.isAuthorizedUser || 'REJECT' === groupActions.status) {
                role.editable = false;
                continue;
            }
            role.editable = this.isRolesEditable;
        }

        /* roleGroups.forEach((roleGroup: any) => {
            if (
                !this.commonUtil.isObject(roleGroup) ||
                roleGroup.isAuthorizedUser
            ) {
                return;
            }
            const group: any = roleGroup.group || {};
            const role: any = roleGroup.role || {};
            role.editable = this.isRolesEditable;
            if (!this.isRolesEditable && group.editable) {
                group.editable = false;
            }
        }); */
    }

    getUserDetails(userId: string) {
        this.userService.getUser(userId).subscribe(
            (response: any) => {
                if (response) {
                    this.user = response;
                    this.contactType = this.commonUtil.getFilteredItem(this.modes, 'code', this.user.preferredContactType) || {};
                    this.editPermission = (this.user.userType === 'CUSTOMER' ||
                        this.user.userType === 'TENANT') &&
                        (this.user.status === 'ACTIVE' ||
                            this.user.status === 'APPROVAL_PENDING')
                        ? this.editPermission : 'NoEditUser';
                    this.isAccountLocked = this.user.locked;
                    // in case of userType CUSTOMER, get authorised logged in users for the customers available
                    if ('CUSTOMER' === this.user.userType || 'TENANT' === this.user.userType) {
                        if ('CUSTOMER' === this.user.userType) {
                            const customers:any[] = this._getAllAssociatedCustomer(this.user.authorizations);
                            if (customers.length === 0 || this._authSvc.userHasPermissions('ApproveUser')) {
                                this.user.authorizations.forEach((authorization) => {
                                    authorization.isAuthorizedUser = true;
                                });
                                this.approveUserPermission = customers.length === 0 ? 'NoApprovePermission' : this.approveUserPermission;
                                this._commonMethodCall();
                            } else {
                                this.userService.checkBulkApprover(customers).subscribe(
                                    (res: string[]) => {
                                        if (this.commonUtil.isValidArray(res)) {
                                            this._authorizedCustomerNumbers = res;
                                            // Enable roles edit only for active users
                                            this._doEnableRolesEdit(this.user.status);
                                            this._markAssociatedCustomersForUpdate(this.user.authorizations);
                                        } else {
                                            this.approveUserPermission = 'NoApprovePermission';
                                        }
                                        this._commonMethodCall();
                                    },
                                    () => {
                                        this._routerSvc.navigate(['/home']);
                                    }
                                );
                            }
                        } else {
                            this._commonMethodCall();
                        }
                    } else {
                        this.usrFrm = new FormGroup({});
                    }
                }
            }
        );
    }

    private _commonMethodCall(): void {
        this._setActionButtons(this.user.status);
        this._disableAddApps();
        if ('CUSTOMER' === this.user.userType) {
            this._setCustomerList();
        }
        this._createUserForm();
    }
    private _markAssociatedCustomersForUpdate(authorizations: any[], updateRoleGroup?:boolean): void {
        const customerNumbers: string[] = this._authorizedCustomerNumbers;
        authorizations.forEach((authorization) => {
            const customerId: string = authorization.data;
            if (-1 !== customerNumbers.indexOf(customerId)) {
                authorization.isAuthorizedUser = true;
            } else {
                authorization.isAuthorizedUser = false;
                this.approveUserPermission = this.user.status === 'APPROVAL_PENDING' &&
                    authorizations.length === 1 ?
                    this.approveUserPermission : 'NoApprovePermission';
            }
            // Update role group
            if (updateRoleGroup) {
                this._updateSelectedRoleGroup(authorization);
            }
        });
    }
    private _removeCachedItem(itemName: string): void {
        if (!this.commonUtil.isValidString(itemName)) {
            return;
        }
        const customerAccountRoles: any[] = this.userService.selectedItems["CUSTOMER_ACC_ROLE"];
        if (!this.commonUtil.isValidArray(customerAccountRoles)) {
            return;
        }
        for (let i = customerAccountRoles.length - 1; i >= 0; --i) {
            if (
                !this.commonUtil.isObject(customerAccountRoles[i])
            ) {
                continue;
            }
            if (itemName === customerAccountRoles[i].name) {
                customerAccountRoles.splice(i, 1);
                break;
            }
        }
    }
    private _setActionButtons(status): void {
        this.actionButtons = [
            {
                disabled: false,
                text: 'APPROVAL_PENDING' === status ? 'COMMON.ACTIONS.REJECT' : 'COMMON.ACTIONS.CANCEL',
                btnClass: 'btn-default',
                action: 'APPROVAL_PENDING' === status ? 'rejectUser' : 'cancel',
                type: 'link',
                isPrimary: false,
                showDeactivate: !this.enableRolesEdit && 'ACTIVE' === status ? true : false,
                order: 1
            },
            {
                disabled: false,
                text: 'new' === status ? 'COMMON.ACTIONS.CREATE_USER' : 'APPROVAL_PENDING' === status ?
                    'COMMON.ACTIONS.APPROVE' : 'COMMON.ACTIONS.UPDATE_USER',
                action: 'new' === status ? 'createUser' : 'APPROVAL_PENDING' === status ?
                    'approveUser' : 'updateUser',
                btnClass: 'pzprimary',
                type: 'button',
                isPrimary: true,
                showDeactivate: status === 'ACTIVE' ? true : false,
                order: 2
            }
        ];
    }

    private _disableAddApps(): void {
        if (
            !this.commonUtil.isValidArray(this.apps) ||
            !this.commonUtil.isObject(this.user)
        ) {
            return;
        }
        const rolesGroups: any[] = this.user.authorizations;
        if (!this.commonUtil.isValidArray(rolesGroups)) {
            return;
        }
        rolesGroups.forEach((rolesGroup) => {
            if (!this.commonUtil.isObject(rolesGroup)) {
                return;
            }
            const item: any = this.commonUtil.getFilteredItem(
                this.apps,
                'name',
                rolesGroup.data
            );
            if (this.commonUtil.isObject(item)) {
                item.hideElement = true;
            }
        });
    }

    private _setCustomerList(): void {
        const authorizations: any[] = this.user.authorizations;
        if (!this.commonUtil.isValidArray(authorizations)) {
            return;
        }
        const custAccounts: any[] = [];
        authorizations.forEach((authorization: any) => {
            const displayName: string = authorization.data + ': ' + authorization.displayName;
            custAccounts.push({
                code: displayName + ': ' + authorization.source,
                name: displayName,
                status: authorization.status,
                hideElement: true
            });
        });
        this.custAccounts = custAccounts;
    }

    onStatusChange() {
        const payload = {
            userId: this.user.userId,
            lockFlag: !this.isAccountLocked
        };
        this.userService.updateLockStatus(payload).subscribe(
            (response) => {
                this.isAccountLocked = !this.isAccountLocked;
                this.user.locked = this.isAccountLocked;
                this._eventEmitter
                    .onAlert
                    .emit({
                        type: 'alert-success',
                        msgKey: 'ALERT.LOCK_STATUS_CHANGED'
                    });
            },
            () => {
                // do not update locked status
            }
        );
    }

    private _createUserForm(): void {
        this._createRoleGroups();
        this.createFormControls();
        this.createFormGroup();
        this.renderForm = true;
    }

    private _createRoleGroups(): void {
        const rolesGroups: any[] = this.user.authorizations;
        if (!this.commonUtil.isValidArray(rolesGroups)) {
            return;
        }
        rolesGroups.forEach((rolesGroup: any) => {
            'CUSTOMER' === this.user.userType ? this.addNewCustomerRole(
                rolesGroup,
                false
            ) : this.addNewAppRole(
                rolesGroup,
                false
            );
        });
    }

    public createFormControls() {
        const roleCards: any = {
            appRoles: [],
            customerAccRoles: []
        };
        for (const key in this.rolesGroup) {
            if (!this.rolesGroup.hasOwnProperty(key)) {
                continue;
            }
            const roleGroups: any[] = this.rolesGroup[key];
            if (!this.commonUtil.isValidArray(roleGroups)) {
                continue;
            }
            roleGroups.forEach((roleGroup: any) => {
                roleGroup.pos = roleGroups.length; // position of forms array in form controls
                roleCards[key].push(
                    new FormGroup({
                        group: roleGroup.group.control,
                        roles: roleGroup.role.control
                    })
                );
            });
            this[key] = new FormArray(roleCards[key]);
        }
    }
    public createFormGroup() {
        const fbGroup: any = {};
        if (this.commonUtil.isValidArray(this.rolesGroup.appRoles)) {
            fbGroup['appRoles'] = this.appRoles;
        }
        if (this.commonUtil.isValidArray(this.rolesGroup.customerAccRoles)) {
            fbGroup['customerAccRoles'] = this.customerAccRoles;
        }
        this.usrFrm = new FormGroup(fbGroup);
    }

    public addNewAppRole(group: any, editable: boolean = true): void {
        const roleGroup: any = {
            roles: group.data === 'promotion' ?
            this.commonUtil.getClonedArray(this.roles.appRoles['PILM']) :
            this.commonUtil.getClonedArray(this.roles.appRoles[group.data]),
            group: {
                label: 'USER_FORM.FIELDS.APP_NAME.LABEL',
                selectCode: 'name',
                displayKey: 'displayName',
                isDropup: true,
                editable: false,
                control: new FormControl(
                    group.data,
                    Validators.required
                )
            },
            role: {
                label: 'USER_FORM.FIELDS.ROLES.LABEL',
                SEARCH_LABEL: 'USER_FORM.FIELDS.ROLES.SEARCH_ROLE',
                isDropup: true,
                editable: false,
                control: new FormControl(group.roles, Validators.required),
                isMultiSelect: true
            }
        };
        const roleGroups: FormArray = this.usrFrm && <FormArray>this.usrFrm.controls['appRoles'];
        if (this.commonUtil.isObject(
            roleGroups
        )
        ) {
            roleGroups.push(
                new FormGroup({
                    group: roleGroup.group.control,
                    roles: roleGroup.role.control
                })
            );
        }
        const item: any = this.commonUtil.getFilteredItem(
            this.apps,
            'selected',
            true
        );
        if (this.commonUtil.isObject(item)) {
            item.hideElement = true;
        }
        this.rolesGroup.appRoles.unshift(roleGroup);
    }

    public addNewCustomerRole(group: any, editable: boolean = true): void {
        if ('INACTIVE' === group.status) {
            return;
        }
        const value: string = this.commonUtil.isValidString(group.displayName) ?
            group.data + ': ' + group.displayName + ': ' + group.source :
            '';
        const roleGroup: any = {
            roles: this.commonUtil.getClonedArray(this.roles.custAccountRoles),
            customerId: group.data,
            group: {
                label: 'USER_FORM.FIELDS.CUST_ACC.LABEL',
                SEARCH_LABEL: 'USER_FORM.FIELDS.CUST_ACC.SEARCH_CUST',
                control: new FormControl(value, Validators.required),
                selectCode: 'code',
                displayKey: 'name',
                isDropup: true,
                editable: editable,
                serverSideSearch: true
            },
            role: {
                label: 'USER_FORM.FIELDS.ROLES.LABEL',
                SEARCH_LABEL: 'USER_FORM.FIELDS.ROLES.SEARCH_ROLE',
                isAuthorizedUser: group.isAuthorizedUser,
                control: new FormControl(group.roles, (editable || (group.isAuthorizedUser && 'REJECT' !== group.status)) ? Validators.required : undefined),
                isMultiSelect: true,
                selectCode: 'code',
                displayKey: 'name',
                isDropup: true,
                editable: editable || this.user.status === 'APPROVAL_PENDING'
            },
            actions: {
                status: this.commonUtil.isValidString(group.status) ? group.status : null,
                isAuthorizedUser: group.isAuthorizedUser,
                canBeRemoved: false,
                userStatus: this.user ? this.user.status : null
            }
        };

        const roleGroups: FormArray = this.usrFrm && <FormArray>this.usrFrm.controls['customerAccRoles'];
        if (this.commonUtil.isObject(
            roleGroups
        )
        ) {
            roleGroup.pos = roleGroups.length; // position of forms array in form controls
            roleGroups.push(
                new FormGroup({
                    group: roleGroup.group.control,
                    roles: roleGroup.role.control
                })
            );
        }
        this.rolesGroup.customerAccRoles.unshift(roleGroup);
    }

    buttonClickedHandler(event) {
        const action: string = event.action;
        if (this.enableRolesEdit) {
            if ('cancel' === action) {
                this.isRolesEditable = false;
                this.markRolesForEdit(action);
                return;
            }
        }
        if (action === 'approveUser' || action === 'deactivateUser' || action === 'rejectUser') {
            const authorization: any = this.user.authorizations ? this.user.authorizations[0] : {};
            if (action === 'rejectUser' || action === 'deactivateUser') {
                event.roles = authorization.roles ? authorization.roles.slice() : [];
            }
            authorization.roles = event.roles.slice();
            event.authorizations = authorization;
            event.status = this.user.status;
            event.customerNo = authorization ? authorization.data : null;
            event.customerName = authorization ? authorization.displayName : null;
        } else if (action === 'approveCustomer' && !this.commonUtil.isValidArray(event.roles)) {
            this.formSubmitted = true;
            return;
        }
        this._showModal(event);
    }

    private _showModal(event: any): void {
        this.actionModal = this._modalService.show(ActionModalComponent, this._modalConfig);
        this.actionModal.content.action = event.action;
        this.actionModal.content.actionKey = 'COMMON.ACTIONS.CONFIRM';
        this.actionModal.content.roles = event.roles ? event.roles.slice() : null;
        this.actionModal.content.customerNo = event.customerNo;
        this.actionModal.content.customerName = event.customerName;
        this.actionModal.content.status = event.status;
        this.actionModal.content.index = event.index;
        switch (event.action) {
            case 'approveUser': {
                this.actionModal.content.titleKey = 'APPROVE_USER';
                this.actionModal.content.reasonReqd = false;
                break;
            }
            case 'rejectUser': {
                this.actionModal.content.titleKey = 'REJECT_USER';
                this.actionModal.content.reasonReqd = true;
                break;
            }
            case 'deactivateUser': {
                this.actionModal.content.titleKey = 'DEACTIVATE_USER';
                this.actionModal.content.reasonReqd = true;
                break;
            }
            case 'approveCustomer': {
                this.actionModal.content.titleKey = 'APPROVE_CUSTOMER';
                this.actionModal.content.reasonReqd = false;
                break;
            }
            case 'rejectCustomer': {
                this.actionModal.content.titleKey = 'REJECT_CUSTOMER';
                this.actionModal.content.reasonReqd = true;
                break;
            }
        }
        const userDetails: any = this.user;
        this.actionModal.content.user = {
            userName: userDetails.firstName + ' ' + (userDetails.lastName ? userDetails.lastName : ''),
            emailId: userDetails.emailId,
            userId: userDetails.userId,
            comment: null
        };
        this.actionModal.content.user.authorizations = event.authorizations;
    }

    private _updateSelectedRoleGroup(authorization:any): void {
        const roleGroups:any[] = this.rolesGroup.customerAccRoles;
        if (!this.commonUtil.isValidArray(roleGroups)) {
            return;
        }
        const customerId:string = authorization.data;
        const groupStatus:string = authorization.status;
        for (let roleGroup of roleGroups) {
            if (!this.commonUtil.isObject(roleGroup) || roleGroup.actions.isAuthorizedUser) {
                return;
            }
            try {
                if (customerId === roleGroup.group.control.value.split(': ')[0]) {
                    roleGroup.actions.isAuthorizedUser = authorization.isAuthorizedUser;
                    roleGroup.actions.status = groupStatus;
                    break;
                }                    
            } catch(e) {
                continue;
            }
        }
    }

    private _updateAssociatedCustomerRoles(): void {
        this.formSubmitted = false;
        const requestPayload: any = this.userService.getRequestPayLoad(this.usrFrm.value, 'CUSTOMER');
        /**
         * @todo: ***
         * Make API calls to save roles related changes
         * Save the response of checkBulkApprover in private and use it here for marking
         * isAuthorizedUser for the newly added customer
         * Check if page refresh can be persisted
         */
        this._updateUserAuthorizations(requestPayload.authorizations);
        this.userService.saveUser(this.user, this.user.userId)
            .subscribe((response: any) => {
                if (!response.status || 0 === response.type) {
                    return;
                }
                if (400 === response.status || 500 === response.status) {
                    this._eventEmitter
                        .onAlert
                        .emit({
                            type: 'alert-error',
                            msgKey: 'ALERT.SERVER_ERR'
                        });
                    return;
                }
                this.user.authorizations = response.body.authorizations;
                this._markAssociatedCustomersForUpdate(this.user.authorizations, true);
                this.isRolesEditable = false;
                this.markRolesForEdit();
                const alertMsg = 'ALERT.USER_UPDATED';
                this._eventEmitter
                    .onAlert
                    .emit({
                        type: 'alert-success',
                        msgKey: alertMsg
                    });
            }, (error: any) => {
                console.log(error);
            });
    }

    _updateUserAuthorizations(updatedAuthorizations: any[]): void {
        const authorizations: any[] = this.user.authorizations;
        if (
            !this.commonUtil.isValidArray(updatedAuthorizations) ||
            !this.commonUtil.isValidArray(authorizations)
        ) {
            return;
        }
        updatedAuthorizations.forEach((roleGroup: any) => {
            const authorization: any = this.commonUtil.getFilteredItem(authorizations, 'data', roleGroup.data);
            if (!this.commonUtil.isObject(authorization)) {
                authorizations.push(roleGroup);
                return;
            }
            authorization.roles = roleGroup.roles;
        });
    }

    _getAllAssociatedCustomer(authorizations):any[] {
        const customers: any[] = [];
        if (
            !this.commonUtil.isValidArray(authorizations)
        ) {
            return customers;
        }
        authorizations.forEach((authorization) => {
            customers.push({
                customerNumber: authorization.data,
                source: authorization.source
            }); 
        });
        return customers;
    }

    toggleNotes() {
        if (!this.collapseCardData.card4.isCollapsed) {
            this.collapseCardData.card4.isCollapsed = true;
            return;
        }
        this.userService.getStatusLogs(this.user.userId).subscribe(
            (res: any) => {
                res = this.commonUtil.isValidArray(res) ? res : [];
                this.collapseCardData.card4.isCollapsed = !this.collapseCardData.card4.isCollapsed;
                this.statusLogs = res.filter((item: any) => {
                    return this.commonUtil.isValidString(item.comments);
                });
                this.commonUtil.sortArrayObjects(this.statusLogs, 'modifiedOn', false, true);
            }
        );
    }

    canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
        return confirm('Do you want to discard the changes?');
    }

    ngOnDestroy() {
        this._actionModalSubscription.unsubscribe();
    }
}
