import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesGroupComponent } from './roles-group.component';

describe('RolesGroupComponent', () => {
  let component: RolesGroupComponent;
  let fixture: ComponentFixture<RolesGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
