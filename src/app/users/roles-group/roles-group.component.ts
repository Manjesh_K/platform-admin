import { Component, EventEmitter, OnInit, Output, Input, OnChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppService } from '../../core/app.service';
import { CommonUtilService } from '../../core/common-util.service';
import { UsersService } from '../users.service';
import { AppConfig } from '../../config/app.config';

@Component({
    selector: 'pz-roles-group',
    templateUrl: './roles-group.component.html',
    styleUrls: ['./roles-group.component.scss']
})
export class RolesGroupComponent implements OnInit {
    @Input()
    public disableActions:boolean;
    @Input()
    public roleGroup: any;
    @Input()
    public fGroup: FormGroup;
    @Input()
    public groupItems: any[];
    @Input()
    public roles: any[];
    @Input()
    public formSubmitted: boolean = false;
    @Input()
    public roleGroupType: string;
    @Input()
    public rowIndex: number;
    @Output()
    public removeRoleGroup: EventEmitter<any> = new EventEmitter();
    public selectedItems: any[] = [];
    private _selectedItem: any;
    constructor(
        private _appSvc: AppService,
        private _userSvc: UsersService,
        private _commonUtilSvc: CommonUtilService,
        private _appConfig: AppConfig
    ) { }
    ngOnInit() {
        const selectedItems = this._userSvc.selectedItems[this.roleGroupType];
        if (this._commonUtilSvc.isValidArray(this.groupItems) || !this._commonUtilSvc.isValidArray(selectedItems)) {
            return;
        }
        this.selectedItems = selectedItems;
        this.groupItems = selectedItems;
    }
    public selectGroupItem(data: any): void {
        const item: any = data.selectedItem;
        if (!this._commonUtilSvc.isObject(data.selectedItem)) {
            return;
        }
        if (this._commonUtilSvc.isObject(this._selectedItem)) {
            this.selectedItems.splice(this.selectedItems.length - 1, 1);
        }
        if (item.selected) {
            const itemsLen: number = this.selectedItems.push(item);
            this._selectedItem = item;
        } else {
            this._selectedItem = undefined;
        }
        if (this.roleGroupType === 'APP_ROLE') {
            if (item.selected) {
                const rolesInfo: any = this._appConfig.getAppFeatureSettings('rolesInfo') || {};
                const roles: any = rolesInfo['APPLICATION'];
                this.roleGroup.roles = item.name === 'promotion'
                ? this._commonUtilSvc.getClonedArray(roles['PILM'])
                : this._commonUtilSvc.getClonedArray(roles[item.name]);
                this.roleGroup.role.editable = true;
                this.roleGroup.role.control.reset();
            } else {
                this.roleGroup.role.control.reset();
                this.roleGroup.role.editable = false;
            }
        }
        this._userSvc.selectedItems[this.roleGroupType] = this.selectedItems;
    }
    public searchCustomers(custNo): void {
        if (!this._commonUtilSvc.isValidArray(this.selectedItems)) {
            this._setSelectedItems();
        }
        this._appSvc.getCustomerList(custNo)
            .subscribe((response: any) => {
                if (!this._commonUtilSvc.isObject(response)) {
                    this.groupItems = [];
                    return;
                }
                const groupItems: any[] = [];
                response.customerInfo.forEach((customer: any) => {
                    const displayValue: string = customer.customerNumber + ': ' + customer.companyName;
                    const groupItem: any = this._commonUtilSvc.getFilteredItem(
                        this.selectedItems, 'code', displayValue + ': ' + customer.source
                    );
                    if (this._commonUtilSvc.isObject(groupItem)) {
                        return;
                    }
                    groupItems.push({
                        code: displayValue + ': ' + customer.source,
                        name: displayValue
                    });
                });
                this.groupItems = groupItems;
            });
    }
    public removeRow() {
        if ('APP_ROLE' === this.roleGroupType) {
            this._removeAppGroup();
            return;
        }
        const eventData: any = this._getActionEmitData();
        eventData.text = 'COMMON.ACTIONS.CONFIRM';
        eventData.action = 'removeCustomer';
        eventData.pos = this.roleGroup.pos;
        this.removeRoleGroup.emit(eventData);
    }
    public approveCustomer() {
        const eventData: any = this._getActionEmitData();
        eventData.text = 'COMMON.ACTIONS.CONFIRM';
        eventData.action = 'approveCustomer';
        this.removeRoleGroup.emit(eventData);
    }

    public rejectCustomer() {
        const eventData: any = this._getActionEmitData();
        eventData.text = 'COMMON.ACTIONS.CONFIRM';
        eventData.action = 'rejectCustomer';
        this.removeRoleGroup.emit(eventData);
    }

    private _getActionEmitData(): any {
        return {
            'index': this.rowIndex,
            'customerNo': (this.roleGroup.group.control.value).split(': ')[0],
            'customerName': (this.roleGroup.group.control.value).split(': ')[1],
            'roles': this.roleGroup.role.control.value,
            'status': this.roleGroup.actions ? this.roleGroup.actions.status : null
        };
    }  
    
    private _getSelectedApp() {
        const group:any = this.roleGroup.group;
        const selectedVal = group.control.value;
        const selectedItem:any = this._commonUtilSvc.getFilteredItem(
            this.groupItems,
            group.selectCode,
            selectedVal
        );
        return this._commonUtilSvc.isObject(selectedItem) ? selectedItem : {};
    }

    private _removeAppGroup(): any {
        const item:any = this._getSelectedApp();
        const displayKey:string = this.roleGroup.group.displayKey;
        this.removeRoleGroup.emit({
            'action': 'REMOVE_APP_ROLES',
            'appCode': this.roleGroup.group.control.value,
            'appName': item[displayKey],
            'index': this.rowIndex,
            'pos': this.roleGroup.pos,
            'roles': this.roleGroup.role.control.value,
            'text': 'COMMON.ACTIONS.CONFIRM',
            'status': this.roleGroup.actions ? this.roleGroup.actions.status : null
        });
    }

    private _setSelectedItems(): void {
        if (!this._commonUtilSvc.isValidArray(this.groupItems)) {
            return;
        }
        this.selectedItems = this.groupItems;
    }
}
