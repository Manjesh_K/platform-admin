import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { SaveUserComponent } from './save-user/save-user.component';
import { CanDeactivateGuard } from '../core/can-deactivate-guard.service';
import { AuthGuard } from '../auth.guard';

const usersRoutes: Routes = [
    {
        path: '', component: UsersComponent,
        canActivate: [AuthGuard],
        data: {
            permission: {
                only: ['ViewUser'],
                redirectTo: 'home'
            }
        }
    },
    {
        path: ':userId/save', component: SaveUserComponent,
        canActivate: [AuthGuard],
        canDeactivate: [CanDeactivateGuard],
        data: {
            permission: {
                only: ['EditUser', 'CreateUser'],
                redirectTo: 'home'
            }
        }
    },
    {
        path: ':userId', component: UserDetailComponent,
        canActivate: [AuthGuard],
        data: {
            permission: {
                only: ['ViewUser'],
                redirectTo: 'home'
            }
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(usersRoutes)
    ],
    exports: [RouterModule]
})
export class UsersRoutingModule {

}
