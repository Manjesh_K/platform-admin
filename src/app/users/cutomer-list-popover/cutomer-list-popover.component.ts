import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'pz-cutomer-list-popover',
  templateUrl: './cutomer-list-popover.component.html',
  styleUrls: ['./cutomer-list-popover.component.scss']
})
export class CutomerListPopoverComponent implements OnInit {
  @Input()
  public customers: any;
  public context: any;
  constructor() { }

  ngOnInit() {
    this.context = {
      'data': this.customers.slice(),
      'title': 'Customer Accounts:'
    };
  }

  togglePopover(event, popover) {
    event.stopPropagation();
  }

}
