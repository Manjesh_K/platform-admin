/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Injectable
} from '@angular/core';
import {
    CommonUtilService
} from './common-util.service';
@Injectable()
export class CacheFactoryService {
    private _pilmCacheFactory: any = {};
    constructor(private _commonUtil: CommonUtilService) {
        this._setCacheBusting();
    }
    /**
     * Deletes a given key from in memory cache
     * @param key
     */
    public deleteItem(key: string): void {
        if (
            !this._commonUtil.isValidString(key)
        ) {
            return;
        }
        delete this._pilmCacheFactory[key];
    }
    /**
     * Clears all cached data
     */
    public deleteAllItems(): void {
        this._pilmCacheFactory = {};
    }
    /**
     * returns cached data of given key
     * @param key
     */
    public getItem(key): any {
        if (
            !this._commonUtil.isValidString(key)
        ) {
            return undefined;
        }
        const response: any = this._pilmCacheFactory[key];
        return this._commonUtil.isObject(response) && response.value || undefined;
    }
    /**
     * Stores a given key value pair in memory cache
     * @param key
     * @param value
     */
    public setItem(key: string, val: any): void {
        if (
            !this._commonUtil.isValidString(key) ||
            this._commonUtil.isUnDefined(val)
        ) {
            return;
        }
        this._pilmCacheFactory[key] = {
            value: val,
            timestamp: new Date().getTime()
        };
    }
    private _setCacheBusting() {
        setTimeout(() => {
            const pilmCache: any = this._pilmCacheFactory;
            for (const key in pilmCache) {
                if (!pilmCache.hasOwnProperty(key)) {
                    continue;
                }
                const value: any = pilmCache[key];
                if (!this._commonUtil.isObject(value)) {
                    continue;
                }
                const timestamp: number = value.timestamp;
                const currentTimestamp: number = new Date().getTime();
                if (900000 >= currentTimestamp - timestamp) {
                    this.deleteItem(key);
                }
            }
            this._setCacheBusting();
        }, 864000000);
    }
}
