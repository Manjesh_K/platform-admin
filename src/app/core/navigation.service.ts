/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Injectable
} from '@angular/core';
import {
    ActivatedRoute,
    NavigationStart,
    NavigationEnd,
    NavigationExtras,
    Params,
    Router
} from '@angular/router';
import { CommonUtilService } from './common-util.service';
import { EventEmitterService } from './event-emitter.service';
// Import Custom Service
@Injectable()
export class NavigationService {
    private _queryParams: Params = {};
    private _routeData: any = {};
    private _routeParams: Params = {};
    private _targetPageUrl: string;
    set queryParams(params: Params) {
        Object.assign(this._queryParams, params);
    }
    set routeData(params: Params) {
        Object.assign(this._routeData, params);
    }
    set routeParams(params: Params) {
        Object.assign(this._routeParams, params);
    }
    constructor(
        private _commonUtil: CommonUtilService,
        private _eventEmitter: EventEmitterService,
        private _router: Router
    ) {
        _router.events.subscribe((event) => {
            if (event instanceof NavigationStart) {
                this._queryParams = {};
                this._routeParams = {};
                this._targetPageUrl = event.url;
            }
            if (event instanceof NavigationEnd) {
                _eventEmitter.onNavigationSuccess.emit(event);
            }
        });
    }
    get targetPageUrl(): string {
        return this._targetPageUrl;
    }
    public getQueryParam(key: string): string {
        if (!this._commonUtil.isValidString(key)) {
            return null;
        }
        const value: string = this._queryParams[key];
        return this._commonUtil.isValidString(value) && value || null;
    }
    public getRouteData(key: string): string {
        if (!this._commonUtil.isValidString(key)) {
            return null;
        }
        const value: string = this._routeData[key];
        return this._commonUtil.isValidString(value) && value || null;
    }
    public getRouteParam(key: string): string {
        if (!this._commonUtil.isValidString(key)) {
            return null;
        }
        const value: string = this._routeParams[key];
        return this._commonUtil.isValidString(value) && value || null;
    }
    public toggleMainContainer(fullScreen: boolean): void {
        this._eventEmitter.onOverlayLoaded.emit(fullScreen);
    }
    public navigate(args: any, qParams?: Params, relativePath?: ActivatedRoute): void {
        const navigationExtras: NavigationExtras = {};
        if (this._commonUtil.isObject(qParams)) {
            navigationExtras.queryParams = qParams;
        }
        if (relativePath instanceof ActivatedRoute) {
            navigationExtras.relativeTo = relativePath;
        }
        this._router.navigate(
            this._commonUtil.isValidArray(args) ? args : [args],
            navigationExtras
        );
    }
}
