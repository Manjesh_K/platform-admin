/*****************************************************************
 * Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
// Import Core Services
import {
    Injectable
} from '@angular/core';
import {
    HttpClient,
    HttpHeaders,
    HttpParams,
    HttpRequest
} from '@angular/common/http';
import {
    Observable
} from 'rxjs/Rx';

// Import Custom Services
import {
    AppConfig
} from '../config/app.config';
import {
    ApiUtilService
} from './api-util.service';
import {
    CacheFactoryService
} from './cache-factory.service';
import {
    CommonUtilService
} from './common-util.service';

@Injectable()
export class ApiDispatcherService extends ApiUtilService {
    private readonly _successCodes: number[] = [
        200,
        201,
        202,
        203,
        204,
        205
    ];
    constructor(
        appConfig: AppConfig,
        commonUtil: CommonUtilService,
        private _http: HttpClient,
        private _cacheFactory: CacheFactoryService
    ) {
        super(appConfig, commonUtil);
    }
    /**
     * Performs a request with `delete` http method.
     * @param apiKey
     * @param routeParams
     * @param params
     * @param headers
     */
    public doDeleteApiCall(
        apiKey: string,
        routeParams?: any,
        httpParams?: any,
        headers?: any,
        endPoint?: string
    ): Observable<any> {
        const apiPath: string = this.getApiPath(apiKey, routeParams, endPoint);
        const options = {
            headers: new HttpHeaders(headers),
            params: httpParams
        };
        return this._http.delete(apiPath, options);
    }
    /**
     * Performs a request with `get` http method.
     * @param apiKey
     * @param routeParams
     * @param params
     * @param headers
     */
    public doGetApiCall(
        apiKey: string,
        routeParams?: any,
        httpParams?: any,
        cache?: boolean,
        headers?: any,
        endPoint?: string
    ): Observable<any> {
        const apiPath: string = this.getApiPath(apiKey, routeParams, endPoint);
        let httpHeaders: HttpHeaders;
        if (cache) {
            httpHeaders = new HttpHeaders({
                'cache': 'true'
            });
        }
        return this._http.get(apiPath, {
            headers: httpHeaders,
            params: httpParams
        });
    }
    /**
     * Performs a request with `post` http method.
     * @param apiKey
     * @param data
     * @param routeParams
     * @param headers
     */
    public doPostApiCall(
        apiKey: string,
        data: any,
        routeParams?: any,
        headers?: any,
        endPoint?: string
    ): Observable<any> {
        const apiPath: string = this.getApiPath(apiKey, routeParams, endPoint);
        const options = {
            headers: new HttpHeaders(headers)
        };
        return this._http.post(apiPath, data, options);
    }
    /**
     * Performs a request with `put` http method.
     * @param apiKey
     * @param data
     * @param routeParams
     * @param headers
     */
    public doPutApiCall(
        apiKey: string,
        data: any,
        routeParams?: any,
        headers?: any,
        endPoint?: string
    ): Observable<any> {
        const apiPath: string = this.getApiPath(apiKey, routeParams, endPoint);
        const options = {
            headers: new HttpHeaders(headers)
        };
        const req = new HttpRequest('PUT', apiPath, data, options);
        return this._http.request(req);
    }

    private refreshToken(): Observable<any> {
        const loginInfo: any = this.commonUtil.getCokie('rememberMe');
        const reqPayload = Object.assign(
            {
                tenant: 'dana.com'
            },
            {
                user: loginInfo.username,
                password: loginInfo.password
            }
        );
        const apiPath: string = this.getApiPath('login', undefined, undefined);
        const req = new HttpRequest('POST', apiPath, reqPayload);
        return this._http.request(req);
    }
}
