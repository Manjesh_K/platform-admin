/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
export class SubscriptionHistory {
    public subscriptions: any[] = [];
    public formFields: any[] = [];
    public setFormFields(formFields: any): void {
        this.formFields = formFields;
    }
    public unsubscribe() {
        this.subscriptions.forEach((subscription: any) => {
            subscription.unsubscribe();
        });
    }
    protected _hasFormField(fieldName: string): boolean {
        try {
            return -1 !== this.formFields.indexOf(fieldName);
        } catch (e) {
            return true;
        }
    }
}
