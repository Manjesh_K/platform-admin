/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Injectable
} from '@angular/core';
import {
    Observable
} from 'rxjs/Observable';
import {
    CookieService
} from 'ngx-cookie-service';
import {
    Subject
} from 'rxjs/Subject';
import {
    FormControl
} from '@angular/forms';

@Injectable()
export class CommonUtilService {
    public isArray: any = Array.isArray;
    public onWindowResize: Subject<number> = new Subject<number>();
    private _footerHeight: number = 0;
    private _headerHeight: number = 0;
    private _timer: any = {};
    get footerHeight() {
        return this._footerHeight;
    }
    set footerHeight(height: number) {
        this._footerHeight = height;
        this.onWindowResize.next();
    }
    get headerHeight() {
        return this._headerHeight;
    }
    set headerHeight(height: number) {
        this._headerHeight = height;
        this.onWindowResize.next();
    }
    constructor(private cookieService: CookieService) { }
    /**
     * Fun: arrayIndexOf
     * @param array of objects
     * @param value is unique value in each object
     * Method to find the index of an object
     * in the given array of objects.
     */
    public arrayIndexOf(arr: any[], prop, value): any | number {
        if (
            !this.isValidArray(arr) ||
            !this.isValidString(prop) ||
            !this.isValidString(value)
        ) {
            return -1;
        }
        for (const i in arr) {
            if (!this.isObject(arr[i])) {
                continue;
            }
            const obj: any = arr[i];
            if (value === obj[prop]) {
                return {
                    idx: i,
                    item: obj
                };
            }
        }
        return -1;
    }

    public delayCallback(id, callback: () => void, timeout: number): void {
        const timerId: number = this._timer[id];
        if (timerId) {
            clearTimeout(timerId);
        }
        this._timer[id] = setTimeout(callback, timeout);
    }
    public convertToArrayObjects(arr: string[]): any[] {
        if (!this.isValidArray(arr)) {
            return;
        }
        const newArr: any[] = [];
        arr.forEach((value: string) => {
            newArr.push({
                name: value,
                code: value
            })
        });
        return newArr;
    }
    /**
     * @param path Cookie path
     * @param domain Cookie domain
     */
    public deleteAllCookies(path?: string, domain?: string): void {
        this.cookieService.deleteAll(path, domain);
    }
    /**
     * @param name Cookie name
     * @param path Cookie path
     * @param domain Cookie domain
     */
    public deleteCokie(name: string, path?: string, domain?: string): void {
        if (!this.isValidString(name)) {
            Observable.throw('Invalid cookie name!');
        }
        this.cookieService.delete(name, path, domain);
    }

    public deSelectTreeChildren(arr: any, key?: string): void {
        const item: any = this.getFilteredItem(
            arr,
            'selected',
            true
        );
        if (this.isObject(item)) {
            item.selected = false;
            const children: any = item[key];
            if (this.isValidString(key) && this.isValidArray(children)) {
                children.forEach((child: any) => {
                    if (child.selected) {
                        child.selected = false;
                    }
                });
            }
        }
    }

    public getClonedArray(arr: any): any[] {
        if (!this.isValidArray(arr)) {
            return [];
        }
        // let obj = Object.assign({}, { data: arr });
        try {
            return JSON.parse(JSON.stringify(arr));
        } catch (e) {
            return arr;
        }
    }

    /**
     * @param name Cookie name
     * @returns {any}
     */
    public getCokie(name: string): any {
        if (!this.isValidString(name)) {
            Observable.throw('Invalid cookie name!');
        }
        if (this.cookieService.check(name)) {
            return this.parseJson(this.cookieService.get(name));
        }
        return undefined;
    }
    /**
     * Return an array of items match with prop and value
     * @param arr
     * @param prop
     * @param value
     */
    public getFilteredItem(arr: any[], prop: string, value: any, allMatches?: boolean): any {
        if (!this.isValidArray(arr)) {
            return null;
        }
        const matches: any[] = arr.filter((item: any) => {
            return value === item[prop];
        });
        return allMatches ? matches : matches[0];
    }
    /**
     * Return an object matching with prop and value
     * @param arr
     * @param prop
     * @param value
     */
    public getFilteredItemWithIndex(arr: any[], prop: string, value: any): any {
        if (!this.isValidArray(arr)) {
            return null;
        }
        for (const i in arr) {
            if (!this.isObject(arr[i])) {
                continue;
            }
            const item: any = arr[i];
            if (value === item[prop]) {
                return {
                    item: item,
                    index: i
                };
            }
        }
    }
    public getSelectedItems(items: any[]): any[] {
        if (!this.isValidArray(items)) {
            return [];
        }
        return items.filter((item: any) => {
            return this.isObject(item) && true === item.selected;
        });
    }
    /**
     * @return: A unique string
     */
    public getUniqueId(prefix: string = ''): string {
        return prefix += Math.random().toString(36).substr(2, 16);
    }
    /**
     * 
     * @param arr 
     * @param key 
     */
    public getUniqueObjects(arr: any[], key: string): any[] {
        if (!this.isValidArray(arr)) {
            return arr;
        }
        if (!this.isValidString(key)) {
            return arr;
        }
        const keys: string[] = [];
        const uniqueArr: any[] = [];
        arr.forEach((item) => {
            const itemKey: string = item[key];
            if (-1 < keys.indexOf(itemKey)) {
                return;
            }
            keys.push(itemKey);
            uniqueArr.push(item);
        })
        return uniqueArr;
    }
    /**
     * Check if a given value is defined
     * @param value
     */
    public isDefined(value: any) {
        return value && 'undefined' !== typeof value;
    }
    /**
     * Check if a given value is undefined
     * @param value
     */
    public isUnDefined(value: any) {
        return !value || 'undefined' === typeof value;
    }

    public isFunction(value: any): boolean {
        return 'function' === typeof value;
    }

    public isObject(value: any): boolean {
        return value !== null && 'object' === typeof value;
    }
    public isValidString(value: any): boolean {
        return 'string' === typeof value && 0 < value.length;
    }
    public isValidArray(value: any): boolean {
        return this.isArray(value) && 0 < value.length;
    }
    public isValidObject(value: any): boolean {
        return this.isObject(value) && Object.keys(value).length > 0;
    }
    public parseJson(str: string): any {
        try {
            return JSON.parse(str);
        } catch (e) {
            console.log('Invalid string.', e);
            return str;
        }
    }
    /**
     * @param name Cookie name
     * @param value Cookie value
     * @param expires Number of days until the cookies expires or an actual `Date`
     * @param path Cookie path
     * @param domain Cookie domain
     * @param secure Secure flag
     */
    public setCookie(
        name: string,
        value: any,
        expires?: any,
        path?: string,
        domain?: string,
        secure?: boolean
    ): void {
        if (!this.isValidString(name)) {
            Observable.throw('Invalid cookie name!');
        }
        if (this.isObject(value)) {
            value = JSON.stringify(value);
        }
        this.cookieService.set(name, value, expires, path, domain, secure);
    }

    /**
     * Fun: traverseTree
     * Author: Irshad
     * @param tree
     * @param limit
     * @param callback
     * @param level
     *
     * Reason To Add traverseTree: treeWalk and treeWalkWithLevel doesn't return parent level info
     */
    public traverseTree(
        tree: any[] | any,
        callback: (obj: any, i: number, level: number) => number | any,
        limit: number,
        level?: number,
        cleanUpLevel?: number
    ): void {
        if (!tree) {
            return;
        }

        if (!this.isValidArray(tree)) {
            tree = [tree];
        }

        if (!level) {
            level = 0; // If the level is not set then start traversing from 0th level
        }

        for (let i: number = 0; i < tree.length; i++) {
            const index: number = callback(tree, i, level);
            if (this.isDefined(index) && index !== i) {
                i = index;
            } else if (level < limit && tree[i].children) {
                ++level;
                this.traverseTree(tree[i].children, callback, limit, level, cleanUpLevel);
                if (cleanUpLevel && !tree[i].children.length && level <= cleanUpLevel) {
                    tree.splice(i, 1);
                    --i;
                }
                --level;
            }
        }
    }

    public replaceUrlParams(url: string, params: any): string {
        if (!this.isValidString(url)) {
            return;
        }
        this.walkTree(params, (value: any, key: string) => {
            const pattern: RegExp = new RegExp(':' + key, 'g');
            url = url.replace(pattern, value);
        });
        return url;
    }

    public resizeWindow() {
        this.onWindowResize.next();
    }

    public walkChildren(obj: any, callback: (item: any) => void, childKey?: string): void {
        if (this.isUnDefined(obj)) {
            Observable.throw('Invalid array / object');
        }
        if (!this.isFunction(callback)) {
            Observable.throw('Invalid callback function.');
        }
        const arr: any[] = !this.isArray(obj) ? [obj] : obj;
        for (const item of arr) {
            callback(item);
            const childArr: any = this.isValidString(childKey) && item[childKey];
            if (this.isValidArray(childArr)) {
                this.walkChildren(childArr, callback, childKey);
            }
        }
    }

    public walkTree(obj: any, callback: (item: any, key: string) => void): void {
        if (!this.isObject(obj)) {
            Observable.throw('Invalid object');
        }
        if (!this.isFunction(callback)) {
            Observable.throw('Invalid callback function.');
        }
        const keys: string[] = Object.keys(obj);
        keys.forEach((key: string) => {
            callback(obj[key], key);
        });
    }

    public setFormDirty(formControls: any) {
        this.walkTree(formControls, (formControl: FormControl) => {
            formControl.markAsTouched();
        });
    }

    public showTooltip(comp: any): void {
        comp.show();
        setTimeout(() => {
            comp.hide();
        }, 5000);
    }

    /**
     * Fun: sortObjectsByKey
     * @param objects
     * @param key
     * @param isInteger
     * @param desc
     * @param isBoolean
     *
     * Method to sort the array of objects based on a particular
     * property of the object provided by the key.
     */
    public sortArrayObjects(
        arr: any[],
        key: string,
        desc?: boolean,
        isInteger?: boolean
    ): any[] {
        return arr.sort((() => {
            return (a, b) => {
                const objectIDA = (isInteger) ? parseInt(a[key], 0) : a[key];
                const objectIDB = (isInteger) ? parseInt(b[key], 0) : b[key];
                if (objectIDA === objectIDB) {
                    return 0;
                }
                if (desc) {
                    return objectIDA > objectIDB ? -1 : 1;
                } else {
                    return objectIDA > objectIDB ? 1 : -1;
                }
            };
        })());
    }

    public sortSelectedItems(arr: any[], key: string): any[] {
        return arr.sort((() => {
            return (item: any): number => {
                if (true === item[key]) {
                    return -1;
                } else {
                    return 1;
                }
            };
        })());
    }

    public updateBtnLink(btn: any, params: any): void {
        if (!this.isObject(btn) || !this.isObject(params)) {
            return;
        }
        this.replaceUrlParams(btn.routerLink, params);
    }

    public updateSaveNextBtn(buttons: any[], disabled: boolean, params?: any): void {
        const btn: any = this.getFilteredItem(
            buttons,
            'type',
            'SAVE_NEXT'
        );
        btn.disabled = disabled;
        if (this.isObject(params) && this.isValidString(btn.href)) {
            btn.routerLink = this.replaceUrlParams(btn.href, params);
        }
    }

    public groupBy(xs, f) {
        if (!this.isValidArray(xs)) {
            return [];
        }
        return xs.reduce((r, v, i, a, k = f(v)) => ((r[k] || (r[k] = [])).push(v), r), {});
    }
}
