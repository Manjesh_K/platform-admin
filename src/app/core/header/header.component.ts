import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { EventEmitterService } from '../event-emitter.service';
import { CommonUtilService } from '../common-util.service';

@Component({
  selector: 'pz-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public fullName: string;
  public isAuthenticated: boolean;
  @Input() collapse;
  constructor(
    private _router: Router,
    private authService: AuthService,
    private eventEmitter: EventEmitterService,
    private commonUtilSvc: CommonUtilService,
    private _authService: AuthService
  ) { }

  ngOnInit() {
    this.isAuthenticated = this.authService.isAuthenticated;
    this._setUserFullName();
    this.eventEmitter.onUserLoginStateChange
      .subscribe(
      (response) => {
        if (response) {
          this.isAuthenticated = true;
          this._setUserFullName();
        } else {
          this.isAuthenticated = false;
        }
      }
    );
    this.eventEmitter.onSessionExpired
      .subscribe(
      (response) => {
        this._authService.redirectionUrl = this._router.url;
        this.onLogout();
      }
    );
  }

  public onHome() {
    const routeTo = this.authService.isAuthenticated ? '/home' : 'signin';
    this._router.navigate([routeTo]);
  }

  public onLogout() {
    const jwtLogin: boolean = this._authService.jwt;
    if (jwtLogin) {
        try {
            window.self.close();
        } catch (e) {
            console.log(e);
        }
      return;
    }
    this.authService.invalidateSession()
      .then(() => {
        this._router.navigate(['/signin']);
      });
  }

  private _setUserFullName() {
    const userInfo = this.authService.getUserInfo();
    const firstName = userInfo.firstName;
    const lastName = userInfo.lastName ? userInfo.lastName : '';
    if (this.commonUtilSvc.isValidString(lastName)) {
      this.fullName = firstName + ' ' + lastName;
    } else {
      this.fullName = firstName;
    }
  }
}
