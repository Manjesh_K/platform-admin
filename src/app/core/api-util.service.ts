/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Observable
} from 'rxjs/Observable';
import {
    AppConfig
} from '../config/app.config';
import {
    CommonUtilService
} from './common-util.service';
import * as restAPIs from '../config/restApi.json';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiUtilService {
    constructor(
        public appConfig: AppConfig,
        public commonUtil: CommonUtilService
    ) { }
    /**
     * Returns a given api path
     * @param apiKey
     * @param params
     * @param endPoint
     */
    public getApiPath(apiKey: string, params: any, endPoint: string): string {
        if (!this.commonUtil.isValidString(apiKey)) {
            Observable.throw('Invalid api key');
        }
        const apiDetails: any = this._getApiDetails(apiKey);
        if (!this.commonUtil.isObject(apiDetails)) {
            return undefined;
        }
        const microServiceName: string = apiDetails['microServiceName'];
        const apiEndPoint: string = this.appConfig.getApiEndPoint(microServiceName);
        const apiPath = this.appConfig.getEnvSettings(
            'protocol') + '//' + apiEndPoint + apiDetails['apiContext'] + apiDetails['apiPath'];
        if (!params || !this.commonUtil.isObject(params)) {
            params = {};
        }
        params['apiVersion'] = apiDetails['apiVersion'];
        return this.commonUtil.replaceUrlParams(apiPath, params);
    }

    private _getApiDetails(apiKey: string): any {
        let apiDetails: any;
        if (!this.commonUtil.isObject(restAPIs)) {
            return;
        }
        for (const microServiceName in restAPIs) {
            if (!restAPIs.hasOwnProperty(microServiceName)) {
                continue;
            }
            apiDetails = this._getApiSettings(apiKey, microServiceName);
            if (this.commonUtil.isObject(apiDetails)) {
                break;
            }
        }
        return apiDetails;
    }

    private _getApiSettings(apiKey: string, microService: string) {
        let apiSettings: any;
        if (!this.commonUtil.isValidString(apiKey)) {
            return {};
        }
        const baseSettings: any = restAPIs[microService];
        const context: string = baseSettings['apiContext'];
        const urls: string[] = this.commonUtil.isObject(baseSettings) && baseSettings['apiUrls'];
        const path: string = this.commonUtil.isObject(urls) && urls[apiKey];
        if (this.commonUtil.isValidString(path)) {
            apiSettings = {
                apiContext: context,
                apiPath: path,
                apiVersion: baseSettings['apiVersion'],
                microServiceName: microService
            };
        }

        return apiSettings;
    }
}
