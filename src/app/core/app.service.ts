import { Injectable } from '@angular/core';
import { ApiDispatcherService } from './api-dispatcher.service';
import {
    Observable
} from 'rxjs/observable';
import { AppConfig } from '../config/app.config';
@Injectable()
export class AppService {
    constructor(
        private _apiDispatcherSvc: ApiDispatcherService,
        private _appConfig: AppConfig
    ) { }
    /**
     * Get list of applications
     */
    getApplicationsList = (): Observable<any> => {
        const tenantName: string = this._appConfig.getConfig('tenantName') || 'dana';
        return this._apiDispatcherSvc.doGetApiCall(
            'applications',
            {
                appName: tenantName
            },
            undefined,
            true
        );
    }
    /**
     * To get the list of counties
     */
    getCountryList(): Observable<any[]> {
        return this._apiDispatcherSvc.doGetApiCall(
            'getCountryList',
            undefined,
            undefined,
            true
        );
    }
    /**
     * To get list of states for a given country
     * @param coutryCode 
     */
    getStateList(coutryCode: string): Observable<any[]> {
        return this._apiDispatcherSvc.doGetApiCall(
            'getStateList',
            undefined,
            {
                code: coutryCode
            },
            true
        );
    }
    /**
     * To get list of cities for a given state
     * @param stateCode 
     */
    getCityList(country: string, state: string): Observable<any[]> {
        return this._apiDispatcherSvc.doGetApiCall(
            'getCityList',
            undefined,
            {
                countrycode: country,
                statecode: state
            },
            true
        );
    }
    /**
     * Get list of customers
     * @param customerNo 
     */
    public getCustomerList(customerNo: string): Observable<any> {
        return this._apiDispatcherSvc.doGetApiCall(
            'getCustomerList',
            {
                custNo: customerNo
            },
            undefined,
            true
        );
    }
}
