/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Manjesh
 */
import {
    Pipe,
    PipeTransform
} from '@angular/core';
import { AppConfig } from '../config/app.config';
import { CommonUtilService } from './common-util.service';
@Pipe({
    name: 'roleFilter'
})
export class RoleFilterPipe implements PipeTransform {
    constructor(
        private _appConfig: AppConfig,
        private _commonUtil: CommonUtilService
    ) {}
    public transform(roleKey: string): string {
        let value: string;
        const rolesInfo: any = this._appConfig.getAppFeatureSettings('rolesInfo') || {};
        const appRoles = rolesInfo['APPLICATION'];
        const custAccountRoles = rolesInfo['CUSTOMER'];
        if (this._commonUtil.isValidObject(custAccountRoles) || this._commonUtil.isValidObject(appRoles)) {
            const appRole = this._commonUtil.getFilteredItem(appRoles, 'code', roleKey, false );
            const custRole = this._commonUtil.getFilteredItem(custAccountRoles, 'code', roleKey, false );
            if (this._commonUtil.isValidObject(appRole)) {
                value = appRole.name;
            } else if (this._commonUtil.isValidObject(custRole)) {
                value = custRole.name;
            } else {
                value = roleKey;
            }
            return value;
        } else {
            return roleKey;
        }
    }
}
