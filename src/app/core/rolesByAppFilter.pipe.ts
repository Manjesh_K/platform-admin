/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Manjesh
 */
import {
    Pipe,
    PipeTransform
} from '@angular/core';
@Pipe({
    name: 'rolesByAppFilter'
})
export class RolesByAppFilterPipe implements PipeTransform {
    public transform(items: any[], filterString: string, propName: string): any[] {
        if (items.length === 0 || !filterString) {
            return [];
        }
        let resultArray = [];
        for (let i = 0; i < items.length; i++) {
            if (items[i]['application'][propName] === filterString) {
                resultArray = (items[i].roles).slice();
                break;
            }
        }
        return resultArray;
    }
}
