/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import {
    Injectable
} from '@angular/core';
import {
    Observable
} from 'rxjs/Observable';
import {
    TranslateService
} from '@ngx-translate/core';
@Injectable()
export class TranslationService {
    constructor(private _translateSvc: TranslateService) { }
    public getTranslation(translationKey: string, params?: any): Observable<any> {
        return this._translateSvc.get(translationKey, params);
    }
}
