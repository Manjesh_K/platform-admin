/*****************************************************************
 * Proprietary & Confidential  |  © 2017 PhaseZero Ventures LLC  *
 * This is part of PhaseZero Ventures LLC and cannot be copied,  *
 * modified and/or distributed without the express permission of *
 * PhaseZero Ventures LLC                                        *
 *****************************************************************/
/**
 * @author: Irshadahmed
 */
import * as CryptoJS from 'crypto-js';
import { CommonUtilService } from './common-util.service';
import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {
    private _AES: any = CryptoJS.AES;
    private _secKEY: string = 'BUILDKEY'; //BUILD.SECKEY;
    constructor(public commonUtil: CommonUtilService) {
    }
    /**
     * Delete given key from local storage
     * @param key
     */
    public deleteItem(key: string): void {
        localStorage.removeItem(key);
    }
    /**
     * Delete all the items present in local storage
     */
    public deleteAllItems(): void {
        localStorage.clear();
    }
    /**
     * Return a give item from local strage
     * @param key
     * @param secure
     */
    public getItem(key: string, secure?: boolean): any {
        if (!this.commonUtil.isValidString(key)) {
            return null;
        }
        if (secure) {
            return this.getSecureItem(key);
        }
        return this.commonUtil.parseJson(localStorage.getItem(key));
    }
    /**
     * Store a given item to local storage
     * @param key
     * @param value
     * @param secure
     */
    public setItem(key: string, value: any, secure?: boolean): void {
        if (!this.commonUtil.isValidString(key)) {
            return;
        }
        if (secure) {
            this.setSecureItem(
                key,
                value
            );
        } else {
            localStorage.setItem(
                key,
                this.commonUtil.isObject(value)
                    ? JSON.stringify(value)
                    : value
            );
        }
    }
    /**
     * Store a given in local storage with encryption
     * @param secureKey
     * @param value
     */
    private setSecureItem(secureKey: string, value: any): void {
        localStorage.setItem(
            secureKey,
            this._AES.encrypt(
                JSON.stringify(value),
                this._secKEY
            )
        );
    }
    /**
     * Return a given item from local storage with decryption
     * @param secureKey
     */
    private getSecureItem(secureKey: string): any {
        const ciphertext: string = localStorage.getItem(secureKey);
        if (!this.commonUtil.isValidString(ciphertext)) {
            return null;
        }
        const bytes = this._AES.decrypt(
            ciphertext.toString(),
            this._secKEY
        );
        const plaintext = bytes.toString(CryptoJS.enc.Utf8);
        const item: any = this.commonUtil.parseJson(
            plaintext
        );
        return item;
    }
}
