import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'pz-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {
  public isCollapse: boolean = false;
  @Output() collapseChange = new EventEmitter();
  @Input()
  get collapse() {
    return this.isCollapse;
  }
  set collapse(val) {
    this.isCollapse = val;
    this.collapseChange.emit(this.isCollapse);
  }
  constructor() { }

  ngOnInit() {
  }

  toggleSideNav() {
    this.collapse = !this.collapse;
  }
}
