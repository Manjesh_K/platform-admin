import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from '../app-routing.module';
import { AuthInterceptor } from '../shared/auth.interceptor';
import { LogingInterceptor } from '../shared/logging.interceptor';
import { ApiDispatcherService } from './api-dispatcher.service';
import { CacheFactoryService } from './cache-factory.service';
import { CommonUtilService } from './common-util.service';
import { NavigationService } from './navigation.service';
import { EventEmitterService } from './event-emitter.service';
import { UsersService } from '../users/users.service';
import { AuthService } from '../auth/auth.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthGuard } from '../auth.guard';
import { LoadingSpinnerComponent } from '../components/loading-spinner/loadingSpinner.component';
import { TranslationService } from './translation.service';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { HomeService } from '../home/home.service';
import { AppService } from './app.service';
import { CanDeactivateGuard } from './can-deactivate-guard.service';
import { ModalModule } from 'ngx-bootstrap/modal/modal.module';
import { AlertComponent } from '../components/alert/alert.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { ComponentsModule } from '../components/components.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
    declarations: [
        HeaderComponent,
        SideNavComponent,
        AlertComponent,
        LoadingSpinnerComponent,
        PageNotFoundComponent
    ],
    imports: [
        CommonModule,
        TranslateModule,
        ComponentsModule,
        AppRoutingModule,
        BsDropdownModule.forRoot(),
        ModalModule.forRoot()
    ],
    exports: [
        HeaderComponent,
        SideNavComponent,
        AlertComponent,
        LoadingSpinnerComponent,
        AppRoutingModule
    ],
    providers: [
        ApiDispatcherService,
        CacheFactoryService,
        CommonUtilService,
        AppService,
        NavigationService,
        EventEmitterService,
        CookieService,
        UsersService,
        HomeService,
        AuthService,
        AuthGuard,
        CanDeactivateGuard,
        TranslationService,
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
        // { provide: HTTP_INTERCEPTORS, useClass: LogingInterceptor, multi: true}
    ]
})
export class CoreModule {}
